package comp2913.web;
//used to get coonection from the database
import com.alibaba.druid.pool.DruidDataSourceFactory;


import javax.sql.DataSource;
import java.sql.*;
import java.util.Properties;

public class Dao_for_mysql {

    private static DataSource ds;
    static{
        try{
            Properties pro = new Properties();
            pro.load(Dao_for_mysql.class.getClassLoader().getResourceAsStream("druid.properties"));
            ds = DruidDataSourceFactory.createDataSource(pro);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static Connection getConn() throws SQLException {
        return ds.getConnection();
    }

    public static void close(Connection conn) throws SQLException {
        if(conn!=null) {

            conn.close();
        }
    }

    public static ResultSet getAll(Connection conn, String sql){
        try {
            Statement as = conn.createStatement();
            ResultSet aa = as.executeQuery(sql);

            return aa;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int delete(Connection conn,String sql){
        try{
            Statement as = conn.createStatement();
            int aa = as.executeUpdate(sql);
            as.close();
            return aa;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int insertOrupdate(Connection conn,String sql, Object[] data){
        try{
            PreparedStatement preState = conn.prepareStatement(sql);

            for(int i=0;i<data.length;i++) {
                preState.setObject(i + 1, data[i]);
            }

            preState.executeUpdate();
            preState.close();
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  0;
    }

}



