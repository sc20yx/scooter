/*
MySQL Backup
Source Server Version: 5.7.17
Source Database: comp2913
Database Connection Version: 8.0.20
Date: 2022/3/13 20:15:42
*/
CREATE DATABASE team21;
USE team21;
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `admin_account` varchar(64) NOT NULL,
  `admin_password` varchar(16) NOT NULL,
  `admin_name` varchar(16) NOT NULL,
  `admin_phone` varchar(16) NOT NULL,
  `admin_email` varchar(32) NOT NULL,
  `admin_type` varchar(16) NOT NULL COMMENT 'include admin and staff',
  `admin_gender` int(10) DEFAULT NULL COMMENT '0 represents female, 1 represents male',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `adress`
-- ----------------------------
DROP TABLE IF EXISTS `adress`;
CREATE TABLE `adress` (
  `adress_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adress_name` varchar(100) NOT NULL,
  `adress_status` bigint(20) NOT NULL DEFAULT '0' COMMENT '0 represent working,1 represent close',
  `adress_location` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`adress_id`),
  KEY `adress_name` (`adress_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `card`
-- ----------------------------
DROP TABLE IF EXISTS `card`;
CREATE TABLE `card` (
  `card_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `card_number` varchar(50) NOT NULL,
  `card_name` varchar(50) NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  PRIMARY KEY (`card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `customer`
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `customer_id` int(20) NOT NULL AUTO_INCREMENT,
  `customer_account` varchar(64) NOT NULL,
  `customer_password` varchar(12) NOT NULL,
  `customer_name` varchar(40) DEFAULT NULL,
  `customer_licence` varchar(40) DEFAULT NULL,
  `customer_email` varchar(255) NOT NULL,
  `customer_phone` varchar(16) NOT NULL,
  `customer_money` int(100) DEFAULT NULL,
  `customer_gender` int(10) DEFAULT NULL COMMENT '0 represents female, 1 represents male',
  `customer_discount` int(100) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `message`
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `message_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `message_type` varchar(100) DEFAULT NULL,
  `message_title` varchar(255) DEFAULT NULL,
  `message_content` varchar(1000) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `message_priority` int(10) NOT NULL COMMENT '12345,1 has 1 the highest priority',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `order`
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `order_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) NOT NULL,
  `scooter_id` bigint(20) NOT NULL,
  `order_time` datetime NOT NULL,
  `order_duration` int(200) NOT NULL,
  `order_pay` int(10) NOT NULL COMMENT '1 represents paid, 0 represents unpaid',
  `order_confirm` int(10) NOT NULL COMMENT '0 represents confirmed, 1 represents unconfirmed',
  `order_money` double(5,3) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
--  Table structure for `stuff_order`
-- ----------------------------
DROP TABLE IF EXISTS `stuff_order`;
CREATE TABLE `stuff_order` (
  `stuff_order_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(100) NOT NULL,
  `customer_identity_number` varchar(100) NOT NULL,
  `customer_phone` varchar(100) NOT NULL,
  `scooter_id` bigint(20) NOT NULL,
  `stuff_order_time` datetime NOT NULL,
  `stuff_order_duration` int(200) NOT NULL,
  `stuff_order_pay` int(10) NOT NULL COMMENT '1 represents paid, 0 represents unpaid',
  `stuff_order_confirm` int(10) NOT NULL COMMENT '0 represents confirmed, 1 represents unconfirmed',
  `stuff_order_money` double(5,3) NOT NULL,
  PRIMARY KEY (`stuff_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `scooter`
-- ----------------------------
DROP TABLE IF EXISTS `scooter`;
CREATE TABLE `scooter` (
  `scooter_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adress_name` varchar(100) NOT NULL,
  `scooter_reserve_time` datetime NOT NULL,
  `scooter_reserve_list` varchar(9000) NOT NULL,
  `scooter_price` int(100) NOT NULL DEFAULT '5' COMMENT '5 pound per hour',
  `scooter_status` varchar(100) NOT NULL DEFAULT '0' COMMENT '0 represents available,1 represents unavailable, 2 represents broken',
  `scooter_reser_status` varchar(100) NOT NULL COMMENT '0,yes,1,no',
  PRIMARY KEY (`scooter_id`),
  KEY `scooter_type_id` (`adress_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Record for `admin`
-- ----------------------------
insert into admin(admin_account,admin_password,admin_name,admin_phone,admin_email,admin_type,admin_gender)values("admin","admin","admin","1234567891","sc20yx@leeds.ac.uk","admin",1);
insert into admin(admin_account,admin_password,admin_name,admin_phone,admin_email,admin_type,admin_gender)values("staff","staff","staff","1234567891","sc20q2h@leeds.ac.uk","staff",1);