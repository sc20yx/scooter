Instruction on installing and building the project and how to deploy the project to IDEA

First, you should download a software named “IntelliJ IDEA 2021.3.2 Ultimate”
And get some jar packages from our GitLab, which are “mysql-connector-java-8.0.20.jar”,”mail.jar”,“junit4.jar”,”druid-1.1.19.jar” and “activation.jar”.

Download the MySQL database(version 8.0.20) from the web. Then configure this MySQL database to your own computer, and open the “comp2913.sql” file in the GitLab follow the instruction in this file to create a correct database. If cannot download version 8.0.20 MySQL database, then please download one great version for MySQL database then please change the “mysql-connector-java-8.0.20.jar” to the same as your MySQL database version

And download the jetty version 9.4.45 and then go to the lib folder and go into the mail folder then change the javax. mail.glassfish jar package into “mail.jar” and “activation.jar” packages. Then go to start.ini file and change “jetty.http.idleTimeout = 30000” to “jetty.http.idleTimeout=300000”

Then create a new project in IDEA select java Enterprise and use the setting for a web application, testing frame use JUnit, use jetty 9.4.45 to be the web application server, and use Maven to be the system. Last, click the continue and click the finish button.


Then delete all files including the folder under “src/main/java” and under ”src/main/webapp” and under”src/main/resource”


Then put the “servlet” folder content under the “src/main/java” folder And put the “web_source” folder content under the” src/main/webapp” folder and put the “resource” folder content under the” src/main/resource” folder 

Then open the project structure for this project, and find the module part, then find the dependency part, then add “mysql-connector-java-8.0.20.jar”, ”mail.jar”, “activation.jar”,”druid-1.1.19.jar” and “junit4.jar” folder, files to the dependency part then tick these four files and click the OK button.

Then open the project structure of the project, click the artifact part, then click the “war exploded” button, after that you can the “mysql-connector-java-8.0.20.jar”,“junit4.jar”,  ”mail.jar”,”druid-1.1.19.jar” and “activation.jar” below the output Layout > Available Elements, then please double click the four jar files, and finally click OK to finish.

Then find the druid.properties file in the resources folder then fill in your MySQL database information in the file, if this step does not fill in the correct data then the application will not be able to access the databases 

Then find the edit configuration in IDEA for jetty and click on it, go into the deployment part and change the content for “use custom context root” to “/java_2913_web” and then go into the server part and change the URL into “http://localhost:8080/java_2913_web”

Please go into the test file and run the test for the database to ensure that the database works correctly, if not work correctly please check the SQL file again. And also please run the two check tests in the test file to ensure that the working file is all in your working directory.
If you want to run the "test_email" file, please fill in your own data in the test file.



At last, can click on the run button to run the project.
