package comp2913.web;
//first get the parameter from JSP file
//then search order for the email JSP
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

@WebServlet(name = "Search_orderByid_Servlet", value = "/Search_orderByid_Servlet")
public class Search_orderByid_Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameter
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String id_For_order = request.getParameter("order_ida");
        String eamil = request.getParameter("oooo");

        request.getSession().setAttribute("previo_id",id_For_order);
        request.getSession().setAttribute("previo_email",eamil);

        //find out the searched order
        ResultSet a = Dao_for_mysql.getAll(conn,"select * from `order` where order_id = "+id_For_order+";");
        try {
            if (a != null){
                if (a.next()) {
                    String nam = a.getString("scooter_id");
                    String nama = a.getString("order_duration");
                    String namaa = a.getString("order_money");
                    Date start = a.getDate("order_time");
                    Object[] order_user = {id_For_order, nama, nam, start, namaa};
                    request.getSession().setAttribute("email_order", order_user);
                }
                a.close();

            }
            conn.close();
            response.sendRedirect("./webpage/stuff/E-mail.jsp");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
