package comp2913.web;
//first get the parameter from JSP file
//then search the scooter id for user to rent
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@WebServlet(name = "Search_scooter_Servlet", value = "/Search_scooter_Servlet")
public class Search_scooter_Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //search useful scooter
        String position = (String)request.getParameter("location");

        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //search the reservable scooter
        ResultSet a = Dao_for_mysql.getAll(conn,"select * from scooter where adress_name = '" + position + "' and scooter_reser_status = 0;");
        List<Object[]> order_user = new ArrayList<Object[]>();
        try {
            if(a != null) {
                while (a.next()) {
                    int id_s = a.getInt("scooter_id");
                    int price_s = a.getInt("scooter_price");
                    order_user.add(new Object[]{id_s, price_s});
                }
                a.close();

            }
            request.getSession().setAttribute("user_reserve_scooter",order_user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        response.sendRedirect("./webpage/select_id.jsp");
    }
}
