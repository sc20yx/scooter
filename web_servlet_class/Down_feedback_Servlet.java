package comp2913.web;
//first search the message from database
//then change the priority for it
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "Down_feedback_Servlet", value = "/Down_feedback_Servlet")
public class Down_feedback_Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameter
        String title1 = request.getParameter("id_lis");
        int intIndex = title1.indexOf(",");
        int loo = 0;
        String[] lista = {"111"};
        if(intIndex == -1){
            lista[0] = title1;
        }else{
            lista = title1.split(",");
        }
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        int pri = 0;
        for(int i = 0; i<lista.length;i++) {
            //find out the feedback in database
            int check = 0;
            ResultSet a = Dao_for_mysql.getAll(conn,"select * from message where message_id = " + lista[i] + ";");
            try {
                //check whether or not there are this username
                if (a != null) {
                    if (a.next()) {
                        pri = a.getInt("message_priority");
                        check = 1;
                    }
                    a.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (check == 0) {
                loo = 1;
                response.sendRedirect("./webpage/failure.jsp");
            }
            if (check == 1) {
                //update data in the feedback database
                Object[] input = {pri + 1, lista[i]};
                Dao_for_mysql.insertOrupdate(conn,"update message set message_priority=? where message_id = ?;", input);
            }
        }
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String ty = request.getParameter("st,ad");
        //check user is staff or customer
        if(loo != 1) {
            if (ty.equals("11")) {
                response.sendRedirect("./webpage/stuff/stuff_feedback.jsp");
            } else {
                response.sendRedirect("./webpage/admin/admin.feedback.jsp");
            }
        }
    }
}
