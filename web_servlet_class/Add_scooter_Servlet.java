package comp2913.web;

//insert a new scooter into database
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;

@WebServlet(name = "Add_scooter_Servlet", value = "/Add_scooter_Servlet")
public class Add_scooter_Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //get current time
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int date = c.get(Calendar.DATE);
        //prepare the data
        String time = year + "/" + month + "/" + date;
        String lis = "0";
        for(int i = 1; i < 4320;i++){
            lis = lis + ",0";
        }
        //insert into database
        Object[] input = {"none",time,0,lis,1,1};
        Dao_for_mysql.insertOrupdate(conn,"insert into scooter(adress_name,scooter_reserve_time,scooter_price,scooter_reserve_list,scooter_status,scooter_reser_status) values (?,?,?,?,?,?);",input);
        response.sendRedirect("./webpage/select_car.jsp");
        try {

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
