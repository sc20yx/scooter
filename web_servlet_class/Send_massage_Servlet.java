package comp2913.web;
//first get the parameter from JSP file
//then sent email to user email
import comp2913.test.test_email;

import javax.activation.DataSource;
import javax.activation.*;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

@WebServlet(name = "Send_massage_Servlet", value = "/Send_massage_Servlet")
public class Send_massage_Servlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameter
        String title = "your order in the team21 project";
        String email_ida = (String)request.getParameter("order_ida");

        String content = (String)request.getParameter("content_email");
        String email = (String)request.getParameter("email_ida");

        int orderCheck = 0;
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ResultSet s = Dao_for_mysql.getAll(conn,"select * from `order` where order_id = " + email_ida +";");
        String orderinfor = "";
        try {
            if(s!= null) {
                if (s.next()) {
                    orderCheck = 1;
                    orderinfor = "order ID:" + email_ida;
                    orderinfor = orderinfor + ", order duration:" + s.getInt("order_duration") + " hour";
                    orderinfor = orderinfor + ", order scooter id:" + s.getInt("scooter_id");
                    orderinfor = orderinfor + ", order time:" + (s.getDate("order_time"));
                    orderinfor = orderinfor + ", order fee:" + s.getInt("order_money") + " pounds";
                }
                s.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(orderCheck == 1) {

            content = orderinfor +""+ content;
            String to = email;

            // serder email
            String from = "2388532829@qq.com";
            // host is localhost
            String host = "smtp.qq.com";
            // get parameter
            Properties properties = System.getProperties();

            properties.setProperty("mail.smtp.auth", "true");
            properties.setProperty("mail.debug", "true");
            properties.setProperty("mail.smtp.timeout", "1000");
            properties.setProperty("mail.smtp.port", "465");
            properties.setProperty("mail.smtp.socketFactory.port", "465");
            properties.setProperty("mail.smtp.socketFactory.fallback", "false");
            properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

            // set email server
            properties.setProperty("mail.smtp.host", host);

            Session session = Session.getInstance(properties, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("2388532829@qq.com", "etpcmjfxiyueeada");
                }
            });

            try {
                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(from));
                message.addRecipient(Message.RecipientType.TO,
                        new InternetAddress(to));
                message.setSubject(title);
                //set massage content
                message.setText(content);
                // send
                Transport.send(message);
                System.out.println("Sent message successfully....");
                response.sendRedirect("./webpage/secussed.jsp");
            } catch (MessagingException mex) {
                mex.printStackTrace();
            }

        }else{
            response.sendRedirect("./webpage/failure.jsp");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
