package comp2913.web;
//delete the order from the database
//and do some recovery for other table in the database

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@WebServlet(name = "Cancel_appointment_Servlet", value = "/Cancel_appointment_Servlet")
public class Cancel_appointment_Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameter
        String username = (String)request.getParameter("order_id");
        String tye = (String)request.getParameter("type");

        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(tye.equals("1")) {
            //when user is customer
            ResultSet ab = Dao_for_mysql.getAll(conn,"select * from `order` where order_id = " + username + ";");
            Calendar orderT = Calendar.getInstance();

            int dura = -1;
            int scooter_id = -1;
            int comfi = -1;
            try {
                if (ab != null) {
                    if (ab.next()) {
                        scooter_id = ab.getInt("scooter_id");
                        orderT.setTime(ab.getDate("order_time"));

                        dura = ab.getInt("order_duration");
                        comfi = ab.getInt("order_confirm");
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                ab.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (comfi == -1) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                response.sendRedirect("./webpage/failure.jsp");
            }
            if (comfi == 1) {
                //not comfirmed order
                Dao_for_mysql.delete(conn,"delete from `order` where order_id = " + username + ";");
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                response.sendRedirect("./webpage/secussed.jsp");
            }
            if (comfi == 0) {
                //comfirmed order
                ResultSet aa = Dao_for_mysql.getAll(conn,"select * from scooter where scooter_id = " + scooter_id + ";");
                try {
                    if (aa.next()) {
                        Calendar scooterT = Calendar.getInstance();
                        scooterT.setTime(aa.getDate("scooter_reserve_time"));

                        String scooter_time = aa.getString("scooter_reserve_list");
                        String[] list_scoote = scooter_time.split(",");

                        //count gap between order time and scooter reserve time
                        long aTime = orderT.getTimeInMillis();
                        long bTime = scooterT.getTimeInMillis();
                        long cTime = aTime-bTime;

                        long sTime=cTime/1000;//second
                        long mTime=sTime/60;//mini
                        long hTime=mTime/60;//hour

                        int gap =  (int)hTime + orderT.get(Calendar.HOUR) + dura ;
                        if(gap > 4320){
                            gap = 0;
                        }
                        int k = 0;
                        if (hTime > 0 ) {
                            k = (int)hTime;
                        }
                        for (int i = k; i < gap; i++) {
                            list_scoote[i] = "0";
                        }
                        //change the string[] to string
                        //update the scooter information
                        String sql_list_scooter = "";
                        for (int i = 0; i < list_scoote.length - 1; i++) {
                            sql_list_scooter = sql_list_scooter + list_scoote[i] + ",";
                        }
                        sql_list_scooter = sql_list_scooter + list_scoote[list_scoote.length - 1];

                        Object[] input_scooter = {sql_list_scooter, scooter_id};
                        Dao_for_mysql.insertOrupdate(conn,"update scooter set scooter_reserve_list=? where scooter_id = ?;", input_scooter);

                        Dao_for_mysql.delete(conn,"delete from `order` where order_id = " + username + ";");
                        try {
                            conn.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        response.sendRedirect("./webpage/secussed.jsp");
                    } else {
                        try {
                            conn.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        response.sendRedirect("./webpage/failure.jsp");
                    }
                    aa.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }else{
            //when user is staff
            ResultSet ab = Dao_for_mysql.getAll(conn,"select * from stuff_order where stuff_order_id = " + username + ";");
            Calendar orderT = Calendar.getInstance();

            int dura = -1;
            int scooter_id = -1;
            int comfi = -1;
            try {
                if (ab != null) {
                    if (ab.next()) {
                        scooter_id = ab.getInt("scooter_id");
                        orderT.setTime(ab.getDate("stuff_order_time"));

                        dura = ab.getInt("stuff_order_duration");
                        comfi = ab.getInt("stuff_order_confirm");
                    }
                    ab.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (comfi == -1) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                response.sendRedirect("./webpage/failure.jsp");
            }
            if (comfi == 1) {
                //not comfirmed order,delete from database
                Dao_for_mysql.delete(conn,"delete from stuff_order where stuff_order_id = " + username + ";");
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                response.sendRedirect("./webpage/secussed.jsp");
            }
            if (comfi == 0) {
                //comfirmed order
                ResultSet aa = Dao_for_mysql.getAll(conn,"select * from scooter where scooter_id = " + scooter_id + ";");
                try {
                    if (aa.next()) {
                        Calendar scooterT = Calendar.getInstance();
                        scooterT.setTime(aa.getDate("scooter_reserve_time"));

                        String scooter_time = aa.getString("scooter_reserve_list");
                        String[] list_scoote = scooter_time.split(",");
                        //count gap between order time and scooter reserve time
                        long aTime = orderT.getTimeInMillis();
                        long bTime = scooterT.getTimeInMillis();
                        long cTime = aTime-bTime;

                        long sTime=cTime/1000;//second
                        long mTime=sTime/60;//mini
                        long hTime=mTime/60;//hour

                        int gap =  (int)hTime + orderT.get(Calendar.HOUR) + dura ;
                        int k = 0;
                        if (hTime > 0) {
                            k = (int)hTime;
                        }

                        for (int i = k; i < gap; i++) {
                            list_scoote[i] = "0";
                        }
                        //change the string[] to string
                        //update the scooter information
                        String sql_list_scooter = "";
                        for (int i = 0; i < list_scoote.length - 1; i++) {
                            sql_list_scooter = sql_list_scooter + list_scoote[i] + ",";
                        }
                        sql_list_scooter = sql_list_scooter + list_scoote[list_scoote.length - 1];

                        Object[] input_scooter = {sql_list_scooter, scooter_id};
                        Dao_for_mysql.insertOrupdate(conn,"update scooter set scooter_reserve_list=? where scooter_id = ?;", input_scooter);
                        try {
                            conn.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        response.sendRedirect("./webpage/secussed.jsp");
                    } else {
                        try {
                            conn.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        response.sendRedirect("./webpage/failure.jsp");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
