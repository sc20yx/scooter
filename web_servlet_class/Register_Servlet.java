package comp2913.web;
//first get the parameter from JSP file
//then check whether this user is in database
//if not in database
//insert into database

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "Register_Servlet", value = "/Register_Servlet")
public class Register_Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameter
        String username = (String)request.getParameter("customer_account");
        String password = (String)request.getParameter("customer_password");
        String lisenc = (String)request.getParameter("customer_licence");
        String name = (String)request.getParameter("customer_name");
        String email = (String)request.getParameter("customer_email");
        String phone = (String)request.getParameter("customer_phone");
        String genders = (String)request.getParameter("sex");

        int gender = Integer.parseInt(genders);
        int check = 0;
        //check whether this user is in database
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ResultSet ab = Dao_for_mysql.getAll(conn,"select * from admin where admin_account = '" + username + "'or admin_name = '" + name +"'or admin_email ='"+email+"';");
        try {
            if(ab!=null) {
                if (ab.next()) {
                    check = 2;
                }
                ab.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ResultSet aab = Dao_for_mysql.getAll(conn,"select * from customer where customer_account = '" + username + "'or customer_name = '" + name +"'or customer_email ='"+email+"';");
        try {
            if(aab!=null) {
                if (aab.next()) {
                    check = 2;
                }
                aab.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        if(check == 2){
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            response.sendRedirect("./webpage/failure.jsp");
        }
        if(check == 0){
            //user is not in database, insert new user into database
            Object[] input = {username,password,name,email,phone,0,gender,lisenc,100};
            Dao_for_mysql.insertOrupdate(conn,"insert into customer (customer_account,customer_password,customer_name,customer_email,customer_phone,customer_money,customer_gender,customer_licence,customer_discount) values (?,?,?,?,?,?,?,?,?);",input);
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            response.sendRedirect("./webpage/secussed.jsp");
        }
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
