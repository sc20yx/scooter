package comp2913.web;
//first get the parameter from JSP file
//then insert a new feedback to the database

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "Feedback_Servlet", value = "/Feedback_Servlet")
public class Send_Feedback_Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameter
        String masstype = (String)request.getParameter("problem_type");
        String title = (String)request.getParameter("problem_detail");
        String content = (String)request.getParameter("message_content");
        int prior = 5;
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //insert a new feedback to database
        Object[] input = {masstype,title,content,prior};
        Dao_for_mysql.insertOrupdate(conn,"insert into message (message_type,message_title,message_content,message_priority) values (?,?,?,?);",input);
        response.sendRedirect("./webpage/secussed.jsp");
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
