package comp2913.web;
//delete the data in session
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet(name = "Logout_Servlet", value = "/Logout_Servlet")
public class Logout_Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //remove session attribute and logout
        request.getSession().removeAttribute("user_information");
        request.getSession().removeAttribute("user__id");
        request.getSession().removeAttribute("user_name");
        request.getSession().removeAttribute("account_type");

        response.sendRedirect("./index.jsp");
    }
}
