package comp2913.web;

//first get the parameter from JSP file
//then delete the feedback from database
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "Solve_feedback_Servlet", value = "/Solve_feedback_Servlet")
public class Solve_feedback_Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String id1 = (String)request.getParameter("id_lis");
        String[] lista = id1.split(",");
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //solve the feedback produced by customer
        for(int i = 0; i<lista.length;i++) {
            int title = Integer.parseInt(lista[i]);
            Dao_for_mysql.delete(conn,"delete from message where message_id = " + title + ";");
        }
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        response.sendRedirect("./webpage/secussed.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
