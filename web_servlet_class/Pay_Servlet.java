package comp2913.web;
//first get the parameter from JSP file
//then change the order state

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "Pay_Servlet", value = "/Pay_Servlet")
public class Pay_Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String user_N = (String)request.getParameter("order_id");
        String tye = (String)request.getParameter("type");

        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (tye!= null) {
            if (tye.equals("0")){
                //if the user is staff
                Object[] input = {1, user_N};
                int b = Dao_for_mysql.insertOrupdate(conn,"update stuff_order set stuff_order_pay = ? where stuff_order_id = ?;", input);
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (b == 0) {
                    response.sendRedirect("./webpage/failure.jsp");
                } else {
                    response.sendRedirect("./webpage/secussed.jsp");
                }
            }

        }else {
            //if user is customer
            Object[] input = {1, user_N};
            int b = Dao_for_mysql.insertOrupdate(conn,"update `order` set order_pay = ? where order_id = ?;", input);
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (b == 0) {
                response.sendRedirect("./webpage/failure.jsp");
            } else {
                response.sendRedirect("./webpage/secussed.jsp");
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
