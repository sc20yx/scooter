package comp2913.web;
//first get the parameter from JSP file
//then check whether this uncomfired book is in database
//if in database
//change the state of the book

import org.junit.Test;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@WebServlet(name = "Book_confirm_Servlet", value = "/Book_confirm_Servlet")
public class Book_confirm_Servlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get the parameter
        String username = (String)request.getParameter("order_id");
        String typ = (String)request.getParameter("type");
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        if(typ.equals("1")) {
            String scooter = "";
            int dura = 0;
            Calendar tim_m = Calendar.getInstance();
            ResultSet ab = Dao_for_mysql.getAll(conn,"select * from `order` where order_id = " + username + ";");
            try {
                if (ab.next()) {
                    scooter = "" + ab.getInt("scooter_id");
                    tim_m.setTime(ab.getDate("order_time"));
                    dura = ab.getInt("order_duration");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                ab.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ResultSet aa = Dao_for_mysql.getAll(conn,"select * from scooter where scooter_id = " + scooter + ";");

            Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int date = c.get(Calendar.DATE);
            //count the gap between now and order time
            long aTime = tim_m.getTimeInMillis();
            long bTime = c.getTimeInMillis();
            long cTime = aTime-bTime;

            long sTime=cTime/1000;//second
            long mTime=sTime/60;//mini
            long hTime=mTime/60;//hour
            long dTime=hTime/24;//day

            int gap_day_ = (int) dTime;
            if (dTime > 180 || dTime < 0) {
                response.sendRedirect("./webpage/failure.jsp");
            }
            else {
                String list_scooter = "";
                Calendar scoo_tt = Calendar.getInstance();
                try {
                    //update data in the scooter
                    if (aa.next()) {
                        scoo_tt.setTime(aa.getDate("scooter_reserve_time"));

                        long aTime1 = c.getTimeInMillis();
                        long bTime1 = scoo_tt.getTimeInMillis();
                        long cTime1 = aTime1 - bTime1;

                        long sTime1 = cTime1 / 1000;//second
                        long mTime1 = sTime1 / 60;//mini
                        long hTime1 = mTime1 / 60;//hour
                        long dTime1 = hTime1 / 24;//day

                        //update the scooter reserve time


                        int month1 = month + 1;
                        //gap between now and scooter reserve time in the half year
                        if (dTime1 > 180 || dTime1 < 0) {
                            String lis = "0";
                            for (int i = 1; i < 4320; i++) {
                                lis = lis + ",0";
                            }

                            String pre_time = year + "/" + month1 + "/" + date;
                            Object[] input = {pre_time, lis, scooter};
                            list_scooter = lis;
                            Dao_for_mysql.insertOrupdate(conn,"update scooter set scooter_reserve_time = ? ,scooter_reserve_list=? where scooter_id = ?;", input);
                        }

                        if (dTime1 > 0 && dTime1 < 180) {
                            String lis = aa.getString("scooter_reserve_list");
                            int i = (int) dTime1;
                            String result_list = lis.substring(i * 24 * 2 - 1);
                            for (int loo = 0; loo < dTime1 * 24; loo++) {
                                result_list = result_list + ",0";
                            }
                            String pre_time = year + "/" + month1 + "/" + date;
                            Object[] input = {pre_time, result_list, scooter};
                            list_scooter = result_list;
                            Dao_for_mysql.insertOrupdate(conn,"update scooter set scooter_reserve_time = ? ,scooter_reserve_list=? where scooter_id = ?;", input);
                        }
                        if (dTime1 == 0) {
                            String lis = aa.getString("scooter_reserve_list");
                            list_scooter = lis;
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                try {
                    aa.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                String[] list_sco = list_scooter.split(",");
                int check = 0;

                for (int lo = gap_day_ * 24 + tim_m.get(Calendar.HOUR); lo < gap_day_ * 24 + tim_m.get(Calendar.HOUR) + dura; lo++) {
                    String s = list_sco[lo];
                    if (s.equals("1")) {
                        check = 1;
                    }
                }
                if (check == 1) {
                    //fail because of the other reserve

                    response.sendRedirect("./webpage/failure.jsp");
                } else {
                    for (int lo = gap_day_ * 24 + tim_m.get(Calendar.HOUR); lo < gap_day_ * 24 + tim_m.get(Calendar.HOUR) + dura; lo++) {
                        list_sco[lo] = "1";
                    }
                    String sql_list_scooter = "";
                    for (int i = 0; i < list_sco.length - 1; i++) {
                        sql_list_scooter = sql_list_scooter + list_sco[i] + ",";
                    }
                    sql_list_scooter = sql_list_scooter + list_sco[list_sco.length - 1];

                    Object[] input_scooter = {sql_list_scooter, scooter};
                    Dao_for_mysql.insertOrupdate(conn,"update scooter set scooter_reserve_list=? where scooter_id = ?;", input_scooter);

                    Object[] input = {0, username};
                    int b = Dao_for_mysql.insertOrupdate(conn,"update `order` set order_confirm = ? where order_id = ?;", input);
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    response.sendRedirect("./webpage/stuff/order_cancel.jsp");
                }
            }
        }else{
            String scooter = "";
            int dura = 0;
            Calendar tim_m = Calendar.getInstance();

            ResultSet ab = Dao_for_mysql.getAll(conn,"select * from stuff_order where stuff_order_id = " + username + ";");
            try {
                if (ab.next()) {
                    scooter = "" + ab.getInt("scooter_id");
                    tim_m.setTime(ab.getDate("stuff_order_time"));
                    dura = ab.getInt("stuff_order_duration");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            ResultSet aa = Dao_for_mysql.getAll(conn,"select * from scooter where scooter_id = " + scooter + ";");

            Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int date = c.get(Calendar.DATE);


            //count the gap between now and order time
            long aTime = tim_m.getTimeInMillis();
            long bTime = c.getTimeInMillis();
            long cTime = aTime-bTime;

            long sTime=cTime/1000;//second
            long mTime=sTime/60;//mini
            long hTime=mTime/60;//hour
            long dTime=hTime/24;//day

            int gap_day_ = (int) dTime;

            //check whether or not the reservation is half year
            if (dTime > 180 || dTime < 0) {
                response.sendRedirect("./webpage/failure.jsp");
            }else {

                String list_scooter = "";
                Calendar scoo_tt = Calendar.getInstance();

                try {
                    if (aa.next()) {
                        scoo_tt.setTime(aa.getDate("scooter_reserve_time"));
                        //count the gap between scooter reserve time and the now
                        long aTime1 = c.getTimeInMillis();
                        long bTime1 = scoo_tt.getTimeInMillis();
                        long cTime1 = aTime1 - bTime1;

                        long sTime1 = cTime1 / 1000;//second
                        long mTime1 = sTime1 / 60;//mini
                        long hTime1 = mTime1 / 60;//hour
                        long dTime1 = hTime1 / 24;//day

                        int month1 = month + 1;
                        //update the scooter reserve time
                        if (dTime1 < 0 || dTime1 >= 180) {
                            String lis = "0";
                            for (int i = 1; i < 4320; i++) {
                                lis = lis + ",0";
                            }
                            String pre_time = year + "/" + month1 + "/" + date;
                            Object[] input = {pre_time, lis, scooter};
                            list_scooter = lis;
                            Dao_for_mysql.insertOrupdate(conn,"update scooter set scooter_reserve_time = ? ,scooter_reserve_list=? where scooter_id = ?;", input);
                        }
                        if (dTime1 > 0 && dTime1 < 180) {
                            String lis = aa.getString("scooter_reserve_list");
                            String result_list = lis.substring((int) dTime1 * 24 * 2 - 1);
                            for (int loo = 0; loo < dTime1 * 24; loo++) {
                                result_list = result_list + ",0";
                            }
                            String pre_time = year + "/" + month1 + "/" + date;
                            Object[] input = {pre_time, result_list, scooter};
                            list_scooter = result_list;
                            Dao_for_mysql.insertOrupdate(conn,"update scooter set scooter_reserve_time = ? ,scooter_reserve_list=? where scooter_id = ?;", input);
                        }
                        if (dTime1 == 0) {
                            String lis = aa.getString("scooter_reserve_list");
                            list_scooter = lis;
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                String[] list_sco = list_scooter.split(",");
                int check = 0;

                for (int lo = gap_day_ * 24 + tim_m.get(Calendar.HOUR); lo < gap_day_ * 24 + tim_m.get(Calendar.HOUR) + dura; lo++) {
                    String s = list_sco[lo];
                    if (s.equals("1")) {
                        check = 1;
                    }
                }
                try {
                    aa.close();
                    ab.close();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (check == 1) {
                    //fail because of the other reserve
                    response.sendRedirect("./webpage/failure.jsp");
                } else {
                    for (int lo = gap_day_ * 24 + tim_m.get(Calendar.HOUR); lo < gap_day_ * 24 + tim_m.get(Calendar.HOUR) + dura; lo++) {
                        list_sco[lo] = "1";
                    }
                    String sql_list_scooter = "";
                    for (int i = 0; i < list_sco.length - 1; i++) {
                        sql_list_scooter = sql_list_scooter + list_sco[i] + ",";
                    }
                    sql_list_scooter = sql_list_scooter + list_sco[list_sco.length - 1];

                    Object[] input_scooter = {sql_list_scooter, scooter};
                    Dao_for_mysql.insertOrupdate(conn,"update scooter set scooter_reserve_list=? where scooter_id = ?;", input_scooter);

                    Object[] input = {0, username};
                    int b = Dao_for_mysql.insertOrupdate(conn,"update stuff_order set stuff_order_confirm = ? where stuff_order_id = ?;", input);
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    response.sendRedirect("./webpage/stuff/order_cancel.jsp");
                }
            }
        }
    }
}
