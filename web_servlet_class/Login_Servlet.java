package comp2913.web;
//first get the parameter from JSP file
//then check whether this user is in database
//if in database
//set some paramater to session

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

@WebServlet(name = "Login_Servlet", value = "/Login_Servlet")
public class Login_Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameter
        String username = (String)request.getParameter("username");
        String password = (String)request.getParameter("password");
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //find out account data from database
        ResultSet a = Dao_for_mysql.getAll(conn,"select * from customer where customer_account = '" + username + "'and customer_password = '" + password +"';");
        int check = 1;
        Calendar c = Calendar.getInstance();
        Calendar order_time = Calendar.getInstance();
        long aTime1 = c.getTimeInMillis();

        try {
            if(a != null) {
                if (a.next()) {
                    //get parameter and set session attribute
                    String nam = a.getString("customer_name");
                    int id_cu = a.getInt("customer_id");
                    String licen_cu = a.getString("customer_licence");
                    String email_cu = a.getString("customer_email");
                    String phone_cu = a.getString("customer_phone");
                    int money_cu = a.getInt("customer_money");
                    int gender_cu = a.getInt("customer_gender");
                    int disc_cu = a.getInt("customer_discount");
                    Object[] user = {id_cu,username,nam,email_cu,phone_cu,gender_cu,disc_cu,licen_cu,money_cu};

                    //check user discount and update
                    int order_duration = 0;
                    ResultSet discount_search = Dao_for_mysql.getAll(conn,"select * from `order` where customer_id = '" + id_cu + "'and order_pay = '" + 1 + "';");
                    if (discount_search != null) {
                        while (discount_search.next()) {

                            order_time.setTime(discount_search.getDate("order_time"));

                            long bTime1 = order_time.getTimeInMillis();
                            long cTime1 = aTime1 - bTime1;

                            long sTime1 = cTime1 / 1000;//second
                            long mTime1 = sTime1 / 60;//mini
                            long hTime1 = mTime1 / 60;//hour
                            long dTime1 = hTime1 / 24;//day

                            if (dTime1 <= 7) {
                                order_duration = order_duration + discount_search.getInt("order_duration");
                            }
                        }
                    }

                    if (order_duration >= 8) {
                        user[6] = 90;
                    }else{
                        user[6] = 100;
                    }


                    request.getSession().setAttribute("user_name", nam);
                    request.getSession().setAttribute("user__id", id_cu);//set user id into session
                    request.getSession().setAttribute("account_type", '0');//0 is customer
                    request.getSession().setAttribute("user_information", user);

                    try {
                        discount_search.close();
                        a.close();
                        conn.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    check = 2;
                    response.sendRedirect("./index.jsp");
                } else {
                    a.close();

                    check = 1;
                }


            }else{
                check = 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(check == 1){
            //get account data from database
            ResultSet ab = Dao_for_mysql.getAll(conn,"select * from admin where admin_account = '" + username + "'and admin_password = '" + password +"';");

            try {
                if(ab!=null) {
                    if (ab.next()) {
                        //get parameter and set session attribute
                        String typ = ab.getString("admin_type");
                        String nam = ab.getString("admin_name");
                        request.getSession().setAttribute("user_name", nam);


                        int id_cu = ab.getInt("admin_id");
                        String email_cu = ab.getString("admin_email");
                        String phone_cu = ab.getString("admin_phone");
                        int gender_cu = ab.getInt("admin_gender");


                        Object[] user = {id_cu,username,nam,email_cu,phone_cu,gender_cu,100,"null",0};
                        request.getSession().setAttribute("user__id", id_cu);


                        //1 is for admin, 2 is staff,0 is customer
                        if (typ.equals("admin")) {
                            request.getSession().setAttribute("account_type", '1');
                        }
                        if (typ.equals("staff")) {
                            request.getSession().setAttribute("account_type", '2');
                        }
                        request.getSession().setAttribute("user_information", user);
                        check = 2;
                        try {
                            ab.close();

                            conn.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        response.sendRedirect("./index.jsp");
                    } else {
                        check = 1;
                    }
                }else{
                    ab.close();

                    check = 1;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if(check == 1){
            try {

                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            response.sendRedirect("./webpage/failure.jsp");
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
