package comp2913.web;
//first get the parameter from JSP file
//then increase the duration for the order

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@WebServlet(name = "Longer_appointment_Servlet", value = "/Longer_appointment_Servlet")
public class Longer_appointment_Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String time1 = (String)request.getParameter("long_time");
        int time = 1;
        if(time1 != null){
            time = Integer.parseInt(time1);
        }

        String user_id = (String)request.getParameter("order_id");

        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        int duration = 0;
        String scooter = "";
        Calendar order_t = Calendar.getInstance();
        ResultSet a = Dao_for_mysql.getAll(conn,"select * from `order` where order_id = " + user_id +";");
        try {
            if(a!=null) {
                if (a.next()) {
                    order_t.setTime(a.getDate("order_time"));
                    duration = a.getInt("order_duration");
                    scooter = a.getString("scooter_id");
                }
            }
            a.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //code for update scooter time
        ResultSet aa = Dao_for_mysql.getAll(conn,"select * from scooter where scooter_id = " + scooter + ";");

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int date = c.get(Calendar.DATE);


        String list_scooter = "";

        try {
            if(aa!=null) {
                if (aa.next()) {
                    Calendar scooter_t = Calendar.getInstance();
                    scooter_t.setTime(aa.getDate("scooter_reserve_time"));
                    //count gap between now and the scooter reserve time
                    long aTime = c.getTimeInMillis();
                    long bTime = scooter_t.getTimeInMillis();
                    long cTime = aTime - bTime;

                    long sTime = cTime / 1000;//second
                    long mTime = sTime / 60;//mini
                    long hTime = mTime / 60;//hour
                    long dTime = hTime / 24;//day

                    int month1 = month + 1;

                    //update scooter information
                    if (dTime < 0 || dTime >= 180) {
                        String lis = "0";
                        for (int i = 1; i < 4320; i++) {
                            lis = lis + ",0";
                        }
                        String pre_time = year + "/" + month1 + "/" + date;
                        Object[] input = {pre_time, lis, scooter};
                        list_scooter = lis;
                        Dao_for_mysql.insertOrupdate(conn,"update scooter set scooter_reserve_time = ? ,scooter_reserve_list=? where scooter_id = ?;", input);
                    }

                    if (dTime > 0 && dTime < 180) {
                        String lis = aa.getString("scooter_reserve_list");
                        String result_list = lis.substring((int) dTime * 24 * 2 - 1);
                        for (int loo = 0; loo < dTime * 24; loo++) {
                            result_list = result_list + ",0";
                        }
                        String pre_time = year + "/" + month1 + "/" + date;
                        Object[] input = {pre_time, result_list, scooter};
                        list_scooter = result_list;
                        Dao_for_mysql.insertOrupdate(conn,"update scooter set scooter_reserve_time = ? ,scooter_reserve_list=? where scooter_id = ?;", input);
                    }
                    if (dTime == 0) {
                        String lis = aa.getString("scooter_reserve_list");
                        list_scooter = lis;
                    }

                }
                aa.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //count gap between order time and now
        long aTime1 = order_t.getTimeInMillis();
        long bTime1 = c.getTimeInMillis();
        long cTime1 = aTime1-bTime1;

        long sTime1=cTime1/1000;//second
        long mTime1=sTime1/60;//mini
        long hTime1=mTime1/60;//hour
        long dTime1=hTime1/24;//day

        if(dTime1 < 0){
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            response.sendRedirect("./webpage/failure.jsp");
        }else {
            String[] list_sco = list_scooter.split(",");
            int check = 0;
            for (int i = (int)dTime1 * 24 + order_t.get(Calendar.HOUR) + duration; i < (int)dTime1 * 24 + order_t.get(Calendar.HOUR) + duration + time; i++) {
                String s = list_sco[i];
                if (s.equals("1")) {
                    check = 1;
                }
            }

            if (check == 1) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                response.sendRedirect("./webpage/failure.jsp");

            } else {

                //update the scooter information

//                for (int lo = (int)dTime1 * 24 + order_t.get(Calendar.HOUR) + duration; lo < (int)dTime1 * 24 + order_t.get(Calendar.HOUR) + duration + time; lo++) {
//                    list_sco[lo] = "0";
//                }
                String sql_list_scooter = "";
                for (int i = 0; i < list_sco.length - 1; i++) {
                    sql_list_scooter = sql_list_scooter + list_sco[i] + ",";
                }
                sql_list_scooter = sql_list_scooter + list_sco[list_sco.length - 1];

                Object[] input_scooter = {sql_list_scooter, scooter};
                Dao_for_mysql.insertOrupdate(conn,"update scooter set scooter_reserve_list=? where scooter_id = ?;", input_scooter);

                duration = duration + time;
                Object[] input = {duration, user_id};
                Dao_for_mysql.insertOrupdate(conn,"update `order` set order_duration = ? where order_id = ?;", input);
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                response.sendRedirect("./webpage/secussed.jsp");
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
