package comp2913.web;
//first get the parameter from JSP file
//then check whether this user information is in database
//if in database
//update the password for the user
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "Update_password_Servlet", value = "/Update_password_Servlet")
public class Update_password_Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameter
        String username = (String)request.getParameter("account_user");
        String password = (String)request.getParameter("password_user");
        String name_user = (String)request.getParameter("name_user");
        String license_user = (String)request.getParameter("license_user");
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        int check = 0;
        ResultSet a = Dao_for_mysql.getAll(conn,"select * from customer where customer_account = '"+ username +"' and customer_name = '"+name_user+"' and customer_licence = '"+license_user+"';");
        try {
            //check whether or not there are this username
            if(a!=null) {
                if (a.next()) {
                    check = 1;
                }
                a.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(check == 1){
            //if database have this user then update the password
            Object[] input = {password,username};
            Dao_for_mysql.insertOrupdate(conn,"update customer set customer_password = ? where customer_account = ?;",input);
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            response.sendRedirect("./webpage/secussed.jsp");

        }
        if(check == 0){
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            response.sendRedirect("./webpage/failure.jsp");
        }
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
