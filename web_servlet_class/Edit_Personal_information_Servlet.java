package comp2913.web;
//first get the parameter from JSP file
//then update the data to database


import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "Personal _information_Servlet", value = "/Personal _information_Servlet")
public class Edit_Personal_information_Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Character aq = (Character) request.getSession().getAttribute("account_type");
        if(aq.equals('0')) {
            //when user is customer
            Object[] uss = (Object[])request.getSession().getAttribute("user_information");
            int idn = (int) uss[0];

            //get parameter
            String username = (String)uss[1];
            String name = (String) request.getParameter("customer_name");
            String email = (String) request.getParameter("customer_email");
            String phone = (String) request.getParameter("customer_phone");
            String gendera = (String) request.getParameter("customer_gender");
            String licen_cu = (String) request.getParameter("customer_licence");
            int gender = Integer.parseInt(gendera);
            //update information in database

            Connection conn = null;
            try {
                conn = Dao_for_mysql.getConn();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            Object[] user = {idn, username, name, email, phone, gender, uss[6], licen_cu, uss[8]};
            request.getSession().setAttribute("user_information", user);
            request.getSession().setAttribute("user_name", name);

            Object[] input = {licen_cu, name, email, phone, gender, username};

            int b = Dao_for_mysql.insertOrupdate(conn,"update customer set customer_licence =?,customer_name=?,customer_email=?,customer_phone=?,customer_gender=? where customer_account = ?;", input);
            if(b == 0){
                response.sendRedirect("./webpage/failure.jsp");
            }else {
                response.sendRedirect("./index.jsp");
            }

        }
        if(aq.equals('1')) {
            //when user is employee
            //get parameter
            Object[] uss = (Object[])request.getSession().getAttribute("user_information");
            int idn = (int) uss[0];
            String username = (String)uss[1];
            String name = (String) request.getParameter("customer_name");
            String email = (String) request.getParameter("customer_email");
            String phone = (String) request.getParameter("customer_phone");
            String gendera = (String) request.getParameter("customer_gender");
            String licen_cu = (String) request.getParameter("customer_licence");
            int gender = Integer.parseInt(gendera);
            //update information in database

            Connection conn = null;
            try {
                conn = Dao_for_mysql.getConn();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            Object[] user = {idn, username, name, email, phone, gender, uss[6], licen_cu, uss[8]};

            request.getSession().setAttribute("user_information", user);
            request.getSession().setAttribute("user_name", name);

            Object[] input = {name, phone,email,gender,username};
            int b = Dao_for_mysql.insertOrupdate(conn,"update admin set admin_name = ?,admin_phone = ?,admin_email=?,admin_gender=? where admin_account = ?;", input);
            if(b == 0){
                response.sendRedirect("./webpage/failure.jsp");
            }else {
                response.sendRedirect("./index.jsp");
            }

        }
        if(aq.equals('2')) {
            //when user is stuff
            Object[] uss = (Object[])request.getSession().getAttribute("user_information");

            int idn = (int) uss[0];
            //get parameter
            String username = (String)uss[1];
            String name = (String) request.getParameter("customer_name");
            String email = (String) request.getParameter("customer_email");
            String phone = (String) request.getParameter("customer_phone");
            String gendera = (String) request.getParameter("customer_gender");
            String licen_cu = (String) request.getParameter("customer_licence");
            int gender = Integer.parseInt(gendera);


            //update information in database
            Connection conn = null;
            try {
                conn = Dao_for_mysql.getConn();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Object[] user = {idn, username, name, email, phone, gender, uss[6], licen_cu, uss[8]};

            request.getSession().setAttribute("user_information", user);
            request.getSession().setAttribute("user_name", name);


            Object[] input = {name, phone,email,gender,username};
            int b = Dao_for_mysql.insertOrupdate(conn,"update admin set admin_name = ?,admin_phone = ?,admin_email=?,admin_gender=? where admin_account = ?;", input);
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if(b == 0){
                response.sendRedirect("./webpage/failure.jsp");
            }else {
                response.sendRedirect("./index.jsp");
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
