package comp2913.web;
//first get the parameter from JSP file
//then according to the staff entered information to insert data to database

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@WebServlet(name = "Staff_book_Servlet", value = "/Staff_book_Servlet")
public class Staff_book_Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //get parameter
        String phone_a = (String)request.getParameter("staf_phone");
        String lisen = (String)request.getParameter("user_lisence");
        String name = (String)request.getParameter("name_user");

        String scooter = (String)request.getParameter("scooter_id");
        Object time = request.getParameter("ordered_time");
        String duration = (String)request.getParameter("ordered_duration");
        double price = 0;
        if(duration.equals("1")){
            price = 2.99;
        }
        if(duration.equals("4")){
            price = 6.99;
        }
        if(duration.equals("24")){
            price = 9.99;
        }
        if(duration.equals("168")){
            price = 25.99;
        }
        //insert the staff order into database

        Object[] input = {name, scooter, time, 0, duration, 1,price,lisen,phone_a};
        Dao_for_mysql.insertOrupdate(conn,"insert into stuff_order(customer_name,scooter_id,stuff_order_time,stuff_order_pay,stuff_order_duration,stuff_order_confirm,stuff_order_money,customer_identity_number,customer_phone) values(?,?,?,?,?,?,?,?,?) ", input);
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        response.sendRedirect("./webpage/secussed.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
