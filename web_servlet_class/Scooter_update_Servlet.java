package comp2913.web;
//first get the parameter from JSP file
//then update the scooter information to the database

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "Scooter_update_Servlet", value = "/Scooter_update_Servlet")
public class Scooter_update_Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameter
        String adress = (String)request.getParameter("position_scoot");
        String reserveForScoot = (String)request.getParameter("reserve_scoot");
        String price1 = (String)request.getParameter("price_scoot");
        int price = Integer.parseInt(price1);

        String status = (String)request.getParameter("status_scoot");
        String id4 = (String)request.getParameter("id_scoot");
        int id = Integer.parseInt(id4);
        //update scooter information
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Object[] input = {adress,reserveForScoot,price,status,id};
        Dao_for_mysql.insertOrupdate(conn,"update scooter set adress_name=?,scooter_reser_status=?,scooter_price=?,scooter_status=? where scooter_id = ?;",input);
        response.sendRedirect("./webpage/select_car.jsp");
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
