package comp2913.web;
//first get the parameter from JSP file
//then according to the user selected to insert data to database

import comp2913.data.Datetime;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@WebServlet(name = "Book_Servlet", value = "/Book_Servlet")
public class User_book_Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameter
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        int id = (int) request.getSession().getAttribute("user__id");
        String scooter = (String)request.getParameter("scooter_id");
        String time = (String)request.getParameter("ordered_time");
        String duration = (String)request.getParameter("ordered_duration");
        String pri = (String)request.getParameter("ordered_price");

        //check whether or not this customer have account
        if(id <= 0) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            response.sendRedirect("./webpage/failure.jsp");
        }else {
            //insert a new order into database
            Object[] input = {id, scooter, time, 0, duration, 1, pri};
            Dao_for_mysql.insertOrupdate(conn,"insert into `order`(customer_id,scooter_id,order_time,order_pay,order_duration,order_confirm,order_money) values(?,?,?,?,?,?,?) ", input);
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            response.sendRedirect("./webpage/secussed.jsp");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
