package comp2913.web;
//first get the parameter from JSP file
//then check whether this card is in database
//if not in database
//insert into database

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "Add_card_Servlet", value = "/Add_card_Servlet")
public class Add_card_Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get the parameter
        String cardname = (String)request.getParameter("card_number");
        String name_card = (String)request.getParameter("card_name");
        int id_user = (int)request.getSession().getAttribute("user__id");
        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Object[] input = {cardname,id_user,name_card};
        //check whether or not card is in the database
        ResultSet ab = Dao_for_mysql.getAll(conn,"select * from card where card_number = '" + cardname + "';");
        int check = 0;
        try {
            if(ab!=null) {
                if (ab.next()) {
                    check = 1;
                }
                ab.close();

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(check == 0) {
            //if card is not in database then insert in database
            Dao_for_mysql.insertOrupdate(conn,"insert into card (card_number,customer_id,card_name) values(?,?,?);", input);
            response.sendRedirect("./webpage/wallet.jsp");
        }
        if(check == 1){
            response.sendRedirect("./webpage/failure.jsp");
        }
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
