package comp2913.web;
//delete the card from database
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "Delete_card_Servlet", value = "/Delete_card_Servlet")
public class Delete_card_Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameter
        String cardname = (String)request.getParameter("cards_number");
        int id_user = (int)request.getSession().getAttribute("user__id");

        Connection conn = null;
        try {
            conn = Dao_for_mysql.getConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //delete card from database
        int ab = Dao_for_mysql.delete(conn,"delete from card where customer_id = "+id_user+" and card_number = " + cardname +";");
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        if(ab != 0) {
            response.sendRedirect("./webpage/wallet2.jsp");
        }else {
            response.sendRedirect("./webpage/failure.jsp");
        }

    }
}
