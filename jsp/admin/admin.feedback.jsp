<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="comp2913.web.Dao_for_mysql" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Connection" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../css/bootstrap.min_feedback.css">
    <link rel="stylesheet" href="../../css/base.css">
    <link rel="stylesheet" href="../../css/feedback.css">
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="../../js/bootstrap.min-feedback.js"></script>
    <title>Feedback</title>
</head>
<body>
<%@ include file="../template/admin_top.jsp"%>
    <div class="container">
        <!-- Navigation end -->
        <div class="main">
            <!-- form start -->
            <div class="form-area" style="padding-left:5%">

                <form action="./admin.feedback.jsp" method="post" name="refre">

                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">Problem Level: </label>
                        <div class="col-md-10">
                            <select name="problem_level" id="problem_level" class="select form-control">
                                <%
                                    String time_a = request.getParameter("problem_level");
                                    if(time_a!=null){
                                        if (time_a.equals("1")) {
                                %>
                                <option value="1"selected="selected">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <%
                                    }if (time_a.equals("2")) {
                                %>
                                <option value="1">1</option>
                                <option value="2" selected="selected">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <%
                                    }if (time_a.equals("3")) {
                                %>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3" selected="selected">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <%
                                    }if (time_a.equals("4")) {
                                %>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4" selected="selected">4</option>
                                <option value="5">5</option>
                                <%
                                    }if (time_a.equals("5")) {
                                %>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5" selected="selected">5</option>
                                <%
                                    }}else{
                                %>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <%
                                    }
                                %>
                            </select>
                        </div>
                    </div>
                </form>

                <!-- checkbox start -->
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">User Feedback: </label>
                    <form action="" method="post" name="check_cont">
                        <div class="col-md-10 checkbox_box">
                            <%
                                Connection conn = Dao_for_mysql.getConn();
                                List<Object[]> order_user = new ArrayList<Object[]>();
                                if(time_a == null) {
                                    ResultSet a = Dao_for_mysql.getAll(conn,"select * from message where message_priority = 1;");
                                    try {
                                        if (a != null) {
                                            while (a.next()) {
                                                String order_id = a.getString("message_id");
                                                String cU_id = a.getString("message_title");
                                                String scooter_id = a.getString("message_type");
                                                String cont_ma = a.getString("message_content");
                                                order_user.add(new Object[]{order_id, scooter_id, cU_id, cont_ma});
                                            }
                                            a.close();
                                        }
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                }else{
                                    ResultSet a = Dao_for_mysql.getAll(conn,"select * from message where message_priority = " +time_a+";");
                                    try {
                                        if (a != null) {
                                            while (a.next()) {
                                                String order_id = a.getString("message_id");
                                                String cU_id = a.getString("message_title");
                                                String scooter_id = a.getString("message_type");
                                                String cont_ma = a.getString("message_content");
                                                order_user.add(new Object[]{order_id, scooter_id, cU_id, cont_ma});
                                            }
                                            a.close();
                                        }
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                }
                                if(order_user==null){
                            %>
                            <div class="custom-control custom-checkbox checkbox_area">
                                <input type="checkbox" class="custom-control-input" id="pd1">
                                <label class="custom-control-label " for="pd1">
                                    Type Of Problem:<br>
                                    Detailed Problem:<br>
                                    Problem Description:<br>
                                </label>
                            </div>
                            <%
                            }else{
                            %>
                            <input type="hidden" name="id_lis" id="id_list">
                            <input type="hidden" name="st,ad" value="00" >
                            <%
                                for(int i = 0; i<order_user.size();i++){
                            %>
                            <div class="custom-control custom-checkbox checkbox_area">
                                <input type="checkbox" name="problem_des" class="custom-control-input" value="<%=order_user.get(i)[0]%>" id="<%=i%>">
                                <label class="custom-control-label" for="<%=i%>">
                                    Type Of Problem:<%=order_user.get(i)[1]%><br>
                                    Detailed Problem:<%=order_user.get(i)[2]%><br>
                                    Problem Description:<%=order_user.get(i)[3]%><br>
                                </label>
                            </div>
                            <%
                                    }
                                }
                            %>
                        </div>
                    </form>
                </div>

                <!-- checkbox end -->
                <div class="btn-area row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8 text-center">
                        <input type="button" class="btn feedback_btn" onclick="refrea()" value="Refresh">
                        <input type="button" class="btn feedback_btn" onclick="solve()" value="solve">

                        <%
                            if(time_a != null){
                                if(!time_a.equals("1")){
                        %>
                        <input type="button" class="btn feedback_btn" onclick="up_gra()" value="upgrade">
                        <%
                                }
                                if(!time_a.equals("5")){
                        %>
                        <input type="button" class="btn feedback_btn" onclick="do_gra()" value="downgrade">
                        <%
                                }
                            }else{
                        %>
                        <input type="button" class="btn feedback_btn" value="upgrade">
                        <input type="button" class="btn feedback_btn" value="downgrade">
                        <%
                            }
                        %>
                    </div>
                </div>
            </div>
            <!-- form end -->
        </div>
    </div>
</body>
<script>
    function solve() {
        var list = document.getElementsByName("problem_des");
        var str = "";
        for(var i = 0; i < list.length;i++){
            if (list[i].checked) {
                if (i == 0) {
                    str = "" + list[0].value;
                } else {
                    str = str + "," + list[i].value;
                }
            }
        }
        if (str.length == 0) {
            window.alert("please select problem");

        } else {
            document.getElementById("id_list").value = str;

            document.check_cont.action = "/java_2913_web/solve_feedback_Servlet";
            document.check_cont.submit();
        }
    }
    function up_gra() {
        var list = document.getElementsByName("problem_des");
        var str = "";
        for(var i = 0; i < list.length;i++){
            if (list[i].checked) {
                if (i == 0) {
                    str = "" + list[0].value;
                } else {
                    str = str + "," + list[i].value;
                }
            }
        }
        if (str.length == 0) {
            window.alert("please select problem");

        } else {
            document.getElementById("id_list").value = str;
            document.check_cont.action = "/java_2913_web/up_feedback_Servlet";
            document.check_cont.submit();
        }
    }
    function do_gra() {
        var list = document.getElementsByName("problem_des");
        var str = "";
        for(var i = 0; i < list.length;i++){
            if (list[i].checked) {
                if (i == 0) {
                    str = "" + list[0].value;
                } else {
                    str = str + "," + list[i].value;
                }
            }
        }
        if (str.length == 0) {
            window.alert("please select problem");

        } else {
            document.getElementById("id_list").value = str;
            document.check_cont.action = "/java_2913_web/down_feedback_Servlet";
            document.check_cont.submit();
        }
    }

    function refrea() {
        document.refre.submit();
    }

</script>
</html>
