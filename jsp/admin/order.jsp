<%@ page import="comp2913.web.Dao_for_mysql" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Connection" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../css/sz_boot.css">
    <link rel="stylesheet" href="../../css/base.css">
    <link rel="stylesheet" href="../../css/order_base.css">
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="../../js/sz_boot.js"></script>
    <title>Order</title>
</head>
<body>
<%@ include file="../template/admin_top.jsp"%>
    <div class="container">
        <!-- Navigation end -->
        <div class="main">
            <div class="row">
                <div class="col-md-7">
                    <!-- Order list page start -->
                        <%
                        Connection conn = Dao_for_mysql.getConn();
                        String time_a = (String)request.getParameter("time_lone");
                        if(time_a == null){
                            time_a = "null";
                        }
                        int total_pri = 0;
                        List<Object[]> order_user = new ArrayList<Object[]>();
                        if(time_a.equals("null")) {
                            ResultSet a = Dao_for_mysql.getAll(conn,"select * from `order` where order_pay = 1;");
                            ResultSet b = Dao_for_mysql.getAll(conn,"select * from stuff_order where stuff_order_pay = 1;");
                            try {
                                if (a != null) {
                                    while (a.next()) {
                                        String order_id = a.getString("order_id");
                                        String cU_id = a.getString("customer_id");
                                        String scooter_id = a.getString("scooter_id");
                                        Date order_time = a.getDate("order_time");
                                        int order_duration = a.getInt("order_duration");
                                        int order_money = a.getInt("order_money");
                                        total_pri = total_pri + order_money;
                                        order_user.add(new Object[]{order_id, scooter_id, order_time, order_duration, order_money, cU_id});
                                    }
                                    a.close();
                                }
                                if (b != null) {
                                    while (b.next()) {
                                        String order_id = b.getString("stuff_order_id");
                                        String cU_id = b.getString("customer_name");
                                        String scooter_id = b.getString("scooter_id");
                                        Date order_time = b.getDate("stuff_order_time");
                                        int order_duration = b.getInt("stuff_order_duration");
                                        int order_money = b.getInt("stuff_order_money");
                                        total_pri = total_pri + order_money;
                                        order_user.add(new Object[]{order_id, scooter_id, order_time, order_duration, order_money, cU_id});
                                    }
                                    b.close();
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }else{
                            ResultSet a = Dao_for_mysql.getAll(conn,"select * from `order` where order_pay = 1 and order_duration = "+time_a+";");
                            ResultSet b = Dao_for_mysql.getAll(conn,"select * from stuff_order where stuff_order_pay = 1 and stuff_order_duration = "+time_a+";");

                            try {
                                if (a != null) {
                                    while (a.next()) {
                                        String order_id = a.getString("order_id");
                                        String cU_id = a.getString("customer_id");
                                        String scooter_id = a.getString("scooter_id");
                                        Date order_time = a.getDate("order_time");
                                        int order_duration = a.getInt("order_duration");
                                        int order_money = a.getInt("order_money");
                                        total_pri = total_pri + order_money;
                                        order_user.add(new Object[]{order_id, scooter_id, order_time, order_duration, order_money, cU_id});
                                    }
                                    a.close();
                                }
                                if (b != null) {
                                    while (b.next()) {
                                        String order_id = b.getString("stuff_order_id");
                                        String cU_id = b.getString("customer_name");
                                        String scooter_id = b.getString("scooter_id");
                                        Date order_time = b.getDate("stuff_order_time");
                                        int order_duration = b.getInt("stuff_order_duration");
                                        int order_money = b.getInt("stuff_order_money");
                                        total_pri = total_pri + order_money;
                                        order_user.add(new Object[]{order_id, scooter_id, order_time, order_duration, order_money, cU_id});
                                    }
                                    b.close();
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }
                        if(order_user!= null){
                            for(int i = 0; i < order_user.size();i++){
                    %>
                    <div class="order-area card">
                        <div class="card-header bg-warning">
                            Order Number:<%=order_user.get(i)[0]%>
                        </div>
                        <div class="card-body text-left bg-light">
                            <div class="row">
                                <div class="col-md-4 data-area">Duration:<%= order_user.get(i)[3]%> </div>
                                <div class="col-md-4 data-area">Customer id:<%= order_user.get(i)[5]%> </div>
                                <div class="col-md-4 data-area">Date time:<%= order_user.get(i)[2]%> </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 data-area"></div>
                                <div class="col-md-4 data-area">Scooter id:<%= order_user.get(i)[1]%> </div>
                                <div class="col-md-4 data-area"><span class="bg-success money-area">Money:<%= order_user.get(i)[4]%> </span></div>
                            </div>
                        </div>
                    </div>
                    <%
                        }
                    }
                    %>
                    <!-- Order list page end -->
                </div>
                <div class="col-md-5">

                    <!-- total income start -->
                    <div class="total-income-area text-center" style="background-color: #ffffff">
                        <h5>Total Income</h5>
                        <div class="income-number-area align-self-center">
                            <%=total_pri +" pounds"%>
                        </div>
                        <form action="./order.jsp" method="post">
                            <div class="text-center" style="margin-top:5%">
                                <input type="submit" value="search" class="btn btn-primary">
                            </div>
                                <br>
                            <div class="col-md-10">
                                <select name="time_lone" id="time" class="select form-control">
                                    <option value="1">1 hour</option>
                                    <option value="4">4 hour</option>
                                    <option value="24">1 day</option>
                                    <option value="168">1 week</option>
                                    <option value="null" selected>All</option>
                                </select>
                            </div>
                        </form>

                        <form action="./chart.jsp" method="post" name="tabl">
                            <input type="hidden" name="time_select_long" id="time_select">
                            <div class="text-center" style="margin-top:5%">
                                <input type="button" value="table" onclick="x()" class="btn btn-primary">
                            </div>
                            <br>
                            <div class="col-md-10">
                                <select name="income_date" id="time_week" class="select form-control">
                                    <option value="day">1 day</option>
                                    <option value="week">1 week</option>
                                    <option value="month">1 month</option>
                                    <option value="year">1 year</option>
                                    <option value="all" selected>All</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                    <!-- total income end -->
            </div>
        </div>
    </div>
</div>
<script>
    function x() {

        document.getElementById("time_select").value = document.getElementById("time").value;

        document.tabl.submit();
    }

</script>
</body>
</html>

