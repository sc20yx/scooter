<%@ page import="comp2913.web.Dao_for_mysql" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.sql.Connection" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="../../js/echarts.min.js"></script>
<script src="../../js/chart_jqeury.js"></script>
<link rel="stylesheet" href="../../css/global.css">
<link rel="stylesheet" href="../../css/style.css">
<html>
<head>
    <title>chart for the income</title>
</head>
<body>
<%@ include file="../template/admin_top.jsp"%>
<header>

</header>

<%

    Connection conn = Dao_for_mysql.getConn();
    String time_a = (String)request.getParameter("time_select_long");
    String income_Long = (String)request.getParameter("income_date");

    if(time_a == null){
        time_a = "null";
    }

    int[] income = {0,0,0,0,0,0,0,0,0,0,0,0};
    Calendar cal = Calendar.getInstance();
    Calendar now = Calendar.getInstance();
    if(time_a.equals("null")) {
        ResultSet a = Dao_for_mysql.getAll(conn,"select * from `order` where order_pay = 1;");
        ResultSet b = Dao_for_mysql.getAll(conn,"select * from stuff_order where stuff_order_pay = 1;");
        try {

            if (a != null) {
                while (a.next()) {
                    cal.setTime(a.getDate("order_time"));

                    long aTime = now.getTimeInMillis();
                    long bTime = cal.getTimeInMillis();
                    long cTime = aTime-bTime;

                    long sTime=cTime/1000;//second
                    long mTime=sTime/60;//mini
                    long hTime=mTime/60;//hour
                    long dTime=hTime/24;//day
                    if(income_Long.equals("day") && dTime <= 1){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + a.getInt("order_money");
                    }if(income_Long.equals("week") && dTime <= 7){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + a.getInt("order_money");
                    }if(income_Long.equals("month") && dTime <= 30){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + a.getInt("order_money");
                    }if(income_Long.equals("year") && dTime <= 365){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + a.getInt("order_money");
                    }if(income_Long.equals("all")){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + a.getInt("order_money");
                    }

                }
                a.close();
            }
            if (b != null) {
                while (b.next()) {
                    cal.setTime(b.getDate("stuff_order_time"));

                    long aTime = now.getTimeInMillis();
                    long bTime = cal.getTimeInMillis();
                    long cTime = aTime-bTime;

                    long sTime=cTime/1000;//second
                    long mTime=sTime/60;//mini
                    long hTime=mTime/60;//hour
                    long dTime=hTime/24;//day
                    if(income_Long.equals("day") && dTime <= 1){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + b.getInt("stuff_order_money");
                    }if(income_Long.equals("week") && dTime <= 7){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + b.getInt("stuff_order_money");
                    }if(income_Long.equals("month") && dTime <= 30){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + b.getInt("stuff_order_money");
                    }if(income_Long.equals("year") && dTime <= 365){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + b.getInt("stuff_order_money");
                    }if(income_Long.equals("all")){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + b.getInt("stuff_order_money");
                    }

                }
                b.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }else{
        ResultSet a = Dao_for_mysql.getAll(conn,"select * from `order` where order_pay = 1 and order_duration = "+time_a+";");
        ResultSet b = Dao_for_mysql.getAll(conn,"select * from stuff_order where stuff_order_pay = 1 and stuff_order_duration = "+time_a+";");

        try {
            if (a != null) {
                while (a.next()) {
                    cal.setTime(a.getDate("order_time"));

                    long aTime = now.getTimeInMillis();
                    long bTime = cal.getTimeInMillis();
                    long cTime = aTime-bTime;

                    long sTime=cTime/1000;//second
                    long mTime=sTime/60;//mini
                    long hTime=mTime/60;//hour
                    long dTime=hTime/24;//day
                    if(income_Long.equals("day") && dTime <= 1){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + a.getInt("order_money");
                    }if(income_Long.equals("week") && dTime <= 7){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + a.getInt("order_money");
                    }if(income_Long.equals("month") && dTime <= 30){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + a.getInt("order_money");
                    }if(income_Long.equals("year") && dTime <= 365){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + a.getInt("order_money");
                    }if(income_Long.equals("all")){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + a.getInt("order_money");
                    }
                }
                a.close();
            }
            if (b != null) {
                while (b.next()) {
                    cal.setTime(b.getDate("stuff_order_time"));

                    long aTime = now.getTimeInMillis();
                    long bTime = cal.getTimeInMillis();
                    long cTime = aTime-bTime;

                    long sTime=cTime/1000;//second
                    long mTime=sTime/60;//mini
                    long hTime=mTime/60;//hour
                    long dTime=hTime/24;//day
                    if(income_Long.equals("day") && dTime <= 1){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + b.getInt("stuff_order_money");
                    }if(income_Long.equals("week") && dTime <= 7){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + b.getInt("stuff_order_money");
                    }if(income_Long.equals("month") && dTime <= 30){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + b.getInt("stuff_order_money");
                    }if(income_Long.equals("year") && dTime <= 365){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + b.getInt("stuff_order_money");
                    }if(income_Long.equals("all")){
                        int month = cal.get(Calendar.MONTH);
                        income[month] = income[month] + b.getInt("stuff_order_money");
                    }
                }
                b.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
%>
<input id="income_datalist" type="hidden" value="<%=income%>">
<div id="main" style="width: 80%;height:60%; float: left"></div>
<script type="text/javascript">
    var elem = document.getElementById("main");
    var data_income = document.getElementById("income_datalist").value;

    var myChart = echarts.init(elem);
    myChart.clear();
    var option = {
        title: {
            text: 'income'
        },
        tooltip: {},
        legend: {
            data:['income money']
        },
        xAxis: {
            type: 'category',
            data: ['January','February','March','April','May','June','July','August','September','October','November','December']
        },
        yAxis: {
            type: 'value'
        },

        series: [{
            name: 'money',
            type: 'bar',
            data: [<%=income[0]%>,<%=income[1]%>,<%=income[2]%>,<%=income[3]%>,<%=income[4]%>,<%=income[5]%>,<%=income[6]%>,<%=income[7]%>,<%=income[8]%>,<%=income[9]%>,<%=income[10]%>,<%=income[11]%>]
        }]
    };

    myChart.setOption(option);

    $(function () {
        $(window).resize(function () {
            var wid = document.body.clientWidth-20;
            var hig = document.body.clientHeight*0.6;
            elem.style.width = wid+"px";
            elem.style.height = hig+"px";
            data_income = document.getElementById("income_datalist").value;

            echarts.init(elem).dispose();

            var myChart = echarts.init(elem);

            var option = {
                title: {
                    text: 'income'
                },
                tooltip: {},
                legend: {
                    data:['income money']
                },
                xAxis: {
                    type: 'category',
                    data: ['January','February','March','April','May','June','July','August','September','October','November','December']
                },
                yAxis: {
                    type: 'value'
                },

                series: [{
                    name: 'money',
                    type: 'bar',
                    data: [<%=income[0]%>,<%=income[1]%>,<%=income[2]%>,<%=income[3]%>,<%=income[4]%>,<%=income[5]%>,<%=income[6]%>,<%=income[7]%>,<%=income[8]%>,<%=income[9]%>,<%=income[10]%>,<%=income[11]%>]
                }]
            };

            myChart.setOption(option);

        })
    })
</script>
</body>
</html>
