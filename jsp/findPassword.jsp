<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="always" name="referrer">
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>findPassword</title>
    <link rel="stylesheet" href="../css/reset.css" type="text/css" />
    <link rel="stylesheet" href="../css/style1.css" type="text/css" />
    <link rel="stylesheet" href="../css/response.css" type="text/css" />

</head>

<body >
<%@ include file="template/top.jsp"%>

    <!--wrapper -->
    <div id="wrapper">
        <!--contentbox  -->

        <div class="content mar">
            <form id="fofo" action="/java_2913_web/update_password_Servlet" method="post">
                <div class="con_list ">
                    <h1 class="mar">Find Password</h1>
                    <ul class="mar">
                        <li>
                            <span class="fl">username:</span>
                            <input id="va1" type="text" name="name_user" class="fl" />
                        </li>
                        <li>
                            <span class="fl">useracoount:</span>
                            <input id="va2" type="text" name="account_user" class="fl" />
                        </li>
                        <li>
                            <span class="fl">license:</span>
                            <input id="va3" type="text" name="license_user" maxlength="16" class="fl" />
                        </li>
<%--                         make text bar to collect information--%>
                        <li>
                            <span class="fl">repassword:</span>
                            <input id="va4" type="password" name="password_user" class="fl" />
                            <input type="button" onclick="x()" value="submit" class="tj" >
                        </li>
                    </ul>
                </div>
            </form>
        </div>
    </div>
</body>
<script>
    function x() {
        var a = document.getElementById("va1").value;
        var a1 = document.getElementById("va2").value;
        var a2 = document.getElementById("va3").value;
        var a3 = document.getElementById("va4").value;
        if (a.length == 0 || a1.length == 0 || a2.length == 0 || a3.length == 0) {
            window.alert("your enter value is empty");
        } else {
            if(a2.length==16) {
                document.getElementById("fofo").submit();
            }else{
                window.alert("your license number is not correct")
            }
        }
    }
</script>
</html>
