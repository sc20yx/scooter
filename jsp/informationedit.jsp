<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html lang="en" dir="ltr">

<head>
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1" />
	<%--    <script src="../js/information.js"></script>--%>
	<link rel="stylesheet" type="text/css" href="../css/information.css">
	<title>Information_edit</title>


</head>
<%
	Character a = (Character) session.getAttribute("account_type");
%>
<body>
<%@ include file="template/trends_top.jsp"%>
<div class="pers">Personal information</div>
<div style="border:2px solid #CCC"></div>

<%
	Object[] user_information = (Object[]) session.getAttribute("user_information");
	if(user_information == null){
		user_information = new Object[]{0,0,0,0,0,0,0,0,0};
		//for get information
	}
%>
<div class="idinf">
<%--person information function for it's can be edit--%>
	<form id="information" action="/java_2913_web/edit_Personal_information_Servlet" method="post">
		<br id="userid">User ID:<input class="userid" name="id_user" type="text" value="<%=user_information[0]%>" disabled="true">
		<br id="useridnum">User Account:<input class="userid" name="customer_account" type="text" value="<%=user_information[1]%>" disabled="true">
		<br>
		<div class="namegroup">
			<div class="fname">Name:</div>
		</div>
		<div class="namegroup2">
			<input class="name" id="va1" name="customer_name" type="text" value="<%=user_information[2]%>" >
		</div>
		Email:
		<br>
		<input class="email" id="va2" name="customer_email" type="text" value="<%=user_information[3]%>" >
		<br>
		Phone number:
		<br>
		<input class="phone" id="va3" maxlength="11" oninput="value=value.replace(/[^\d]/g,'')" name="customer_phone" type="text" value="<%=user_information[4]%>" >

		<br>
		Gender
		<br>
<%--		Gender choose and check--%>
		<%
			if((int)user_information[5] == 0){
		%>
		<select id="gender" name="customer_gender">
			<option value=1>Male</option>
			<option value=0 selected="selected">Female</option>
			<option value=2>Neutral</option>
		</select>
		<%
			}
			if((int)user_information[5] == 1){
		%>
		<select id="gender" name="customer_gender">
			<option value=1 selected="selected">Male</option>
			<option value=0>Female</option>
			<option value=2>Neutral</option>
		</select>
		<%
			}
			if((int)user_information[5] != 1 && (int)user_information[5] != 0){
		%>
		<select id="gender" name="customer_gender">
			<option value=1>Male</option>
			<option value=0>Female</option>
			<option value=2 selected="selected">Neutral</option>
		</select>
		<%
			}
		%>
		<br>
		Discount:
		<br>
		<input class="discount" name="customer_discount" type="text" value="<%=user_information[6]%>" disabled="true">

		<br>
		Driving Licence number:
		<br>
		<input class="drivenum"  name="customer_licence" maxlength="16" id="driving" type="text" value="<%=user_information[7]%>" >
		<div class="wallet">
			Balance:
<%--			get the moneny data from sql --%>
			<br>
			<input class="balance" type="text" name="customer_money" value="<%=user_information[8]%>" disabled="true">
			<%
				if (a!=null){
				if(a.equals('0')){
			%>
			<buttom class="buttonwallet"><a href="wallet2.jsp">My Wallet</a></buttom>
			<%
				}
				}
			%>
		</div>
		<br>
		<input class="button" type="button" onclick="x()" value="Submit">
	</form>
</div>
</body>
<script>
//submit function
	function x() {
		var a = document.getElementById("va1").value;
		var a1 = document.getElementById("va2").value;
		var a2 = document.getElementById("va3").value;
		var a3 = document.getElementById("driving").value;
		var a4 = document.getElementById("gender").value;

		if (a.length == 0 || a1.length == 0 || a2.length == 0 || a3.length == 0 || a4.length == 0) {
			window.alert("your enter value is empty");
		} else {
			if(a3.length == 16) {
				if (a2.length != 10 && a2.length != 11) {
					window.alert("your phone is not correct");
				} else {
					document.getElementById("information").submit();
				}
			}else{
				window.alert("your license is not correct");

			}
		}
	}
	// header Responsive function
	function myFunction() {
		document.getElementsByClassName("downMenu")[0].classList.toggle("responsive");
	}
</script>


</html>
