<%@ page import="comp2913.web.Dao_for_mysql" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Connection" %>
<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>wallet</title>
    <!-- <link rel="stylesheet" href="./css/common.css"> -->
    <link rel="stylesheet" href="../css/wallet.css">
</head>

<body>
<%@ include file="template/top.jsp"%>

    <div class="w">
        <h2>wallet</h2>
        <div class="content">
            <%--get the innformation from database--%>
            <div class="left_box">
                <ul>
                    <%
                        Connection conn = Dao_for_mysql.getConn();
                        int id_user = (int)request.getSession().getAttribute("user__id");

                        ResultSet a = Dao_for_mysql.getAll(conn,"select * from card where customer_id = '"+id_user+"';");
                        List<Object[]> cards = new ArrayList<Object[]>();
                        try {
                            if(a != null) {
                                while (a.next()) {
                                    String card_num = a.getString("card_number");
                                    String card_na = a.getString("card_name");
                                    cards.add(new Object[]{card_na,card_num});
                                }
                            }
                            a.close();
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        conn.close();
                        if(a == null){

                        }else{
                            for(int i = 0;i< cards.size();i++){

                    %>
                    <li class="first">
                        <%--get the innformation of first card from database--%>
                        <form action="/java_2913_web/delete_card_Servlet" method="post">
                            <div class="info">
                                <input  class="card" type="text" name="cards_name" value="<%=cards.get(i)[0]%>" readonly="readonly">
                                <br>
                                <br>
                                <input class="card" type="text" name="cards_number" value="<%=cards.get(i)[1]%>" readonly="readonly">
                                <br>
                                <input type="submit" class="edit" value="Delete">
                            </div>
                        </form>
                    </li>
                    <%
                            }
                        }
                    %>
                    <li class="second">
                        <%--get the innformation of second card from database--%>
                        <div class="info">
                            <a href="./wallet.jsp" target="_blank">Add credit</a>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>

</body>
<script>
    function myFunction() {
        document.getElementsByClassName("downMenu")[0].classList.toggle("responsive");
    }
</script>


</html>
