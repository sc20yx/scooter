<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home Page</title>
    <link rel="stylesheet" href="./css/global.css">
    <link rel="stylesheet" href="./css/style.css">
    <style>

    </style>
</head>
<body>
<%@ include file="webpage/template/trends_top.jsp"%>

    <!-- end -->

    <!-- subject starts -->
    <section class="content">
        <!-- carousel -->
        <section class="banner">
            <ul class="slides">
                <input type="radio" id="control-1" name="control" checked>
                <input type="radio" id="control-2" name="control">

                <li class="slide">
                    <img src="./image/1.jpg">

                </li>
                <li class="slide">
                    <img src="./image/2.jpg">
                </li>


                <div class="controls-visible">
                    <label for="control-1"></label>
                    <label for="control-2"></label>
                </div>
            </ul>

        </section>
        <!-- carousel end -->

        <!-- words5 -->
        <section class="passage">
            <p>We are committed to facilitating people's travel and contributing to the cause of environmental protection! </p>
        </section>
        <!-- words end -->


        <!-- help -->
        <section class="help">
            <div class="title">
                <h3>How can we help you today?</h3>
            </div>
            <div class="support">
                <%
                    Character a = (Character) session.getAttribute("account_type");
                    if(a != null){
                    if(a.equals('2')){
                %>
                <span><a href="./webpage/serverDate.jsp"><img src="./image/bookScooter.png" alt=""></a></span>
                <span><a href="./webpage/stuff/order_cancel.jsp"><img src="./image/viewYourOrder.png" alt=""></a></span>
                <span><a href="./webpage/stuff/stuff_feedback.jsp"><img src="./image/askForHelp.png" alt=""></a></span>
                <%
                    }else{
                        if(a.equals('0')){
                %>
                <span><a href="./webpage/rent.jsp"><img src="./image/bookScooter.png" alt=""></a></span>
                <span><a href="./webpage/user/order_not_start.jsp"><img src="./image/viewYourOrder.png" alt=""></a></span>
                <span><a href="./webpage/user/user.feedback.jsp"><img src="./image/askForHelp.png" alt=""></a></span>

                <%
                }else{
                %>

                <span><img src="./image/bookScooter.png" alt=""></span>
                <span><img src="./image/viewYourOrder.png" alt=""></span>
                <span><img src="./image/askForHelp.png" alt=""></span>

                <%
                        }
                    }
                    }else{
                %>
                <span><img src="./image/bookScooter.png" alt=""></span>
                <span><img src="./image/viewYourOrder.png" alt=""></span>
                <span><img src="./image/askForHelp.png" alt=""></span>

                <%
                    }
                %>

            </div>

        </section>

        <!-- help end -->
        <!-- announcement -->
        <section class="announcement">
            <div class="title" >
                <h3 style="color:black;">Announcement:</h3>
            </div>
            <div class="anno-up">
                <div class="anno-up-l">
                    <div class="words">
                        <span>Discount For:</span>
                        <span>frequent users (8+hrs per week)</span>
                    </div>
                </div>
                <div class="anno-up-r">
                    <div class="words">
                        <span> Fee information:</span>
                        <span>1hour(2.99)</span>
                        <span>4hours (6.99)</span>
                        <span>1 day (9.99)</span>
                        <span>1 week (25.99)</span>
                    </div>
                </div>
            </div>
            <div class="anno-down">
                <div class="words">
                    <span>Existing Site:</span>
                    <span>Trinity centre&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LRl hospital</span>
                    <span>Train station&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;UoLEdge sports centre</span>
                    <span>Merrion centre <br></span>
                </div>
            </div>
        </section>
        <!-- announcement end -->
    </section>
    <!-- all end -->

</body>
</html>
