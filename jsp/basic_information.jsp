<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="stylesheet" type="text/css" href="../css/information.css">
    <title>Information</title>
</head>
<body>
<%@ include file="template/trends_top.jsp"%>

<%
    Character a = (Character) session.getAttribute("account_type");
%>
<div class="pers">Personal information</div>
<div style="border:2px solid #CCC"></div>
<div class="idinf">

    <%
        Object[] user_information = (Object[]) session.getAttribute("user_information");
        if (user_information == null) {
            user_information = new Object[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        }
        //for get the information from sql
    %>
    <%--          for personal information--%>
    <br id="userid">User ID:<input class="userid" type="text" value="<%=user_information[0]%>" disabled="true">
    <br id="useridnum">User Account:<input class="userid" type="text" value="<%=user_information[1]%>" disabled="true">
    <br>
    <div class="namegroup">
        <div class="fname">
            Name:
        </div>
    </div>
    <div class="namegroup2">
        <%--            get the perosonal information--%>
        <input class="name" name="name" type="text" value="<%=user_information[2]%>" disabled="true">
    </div>
    Email:
    <br>
    <input class="email" name="email" id="" type="text" value="<%=user_information[3]%>" disabled="true">
    <br>
    Phone number:
    <br>
    <input class="phone" type="text" value="<%=user_information[4]%>" disabled="true">

    <br>
    Gender
    <%--          for choose the Gender--%>
    <br>
    <%
        if ((int) user_information[5] == 0) {
    %>
    <select id="gender" disabled="true">
        <option>Male</option>
        <option selected="selected">Female</option>
        <option>Neutral</option>
    </select>
    <%
        }
        if ((int) user_information[5] == 1) {
    %>
    <select id="gender" disabled="true">
        <option selected="selected">Male</option>
        <option>Female</option>
        <option>Neutral</option>
    </select>
    <%
        }
        if ((int) user_information[5] != 1 && (int) user_information[5] != 0) {
    %>
    <select id="gender" disabled="true">
        <option>Male</option>
        <option>Female</option>
        <option selected="selected">Neutral</option>
    </select>
    <%
        }
    %>
    <br>
    Discount:
    <br>
    <input class="discount" type="text" value="<%=user_information[6]%>" disabled="true">
    <br>
    Driving Licence number:
    <br>
    <input class="drivenum" id="driving" type="text" value="<%=user_information[7]%>" disabled="true">
    <div class="wallet">
        Balance:
        <br>
        <input class="balance" type="text" value="<%=user_information[8]%>" disabled="true">
        <%
            if (a != null) {
                if (a.equals('0')) {
        %>
        <button class="buttonwallet"><a href="wallet2.jsp">My Wallet</a></button>
        <%
                }
            }
        %>
    </div>
    <br>
    <%--          button for edit the information or logout--%>
    <button class="button" style="margin-bottom: 10px;box-shadow:unset !important;"><a href="./informationedit.jsp" >Edit</a></button>
    <form action="/java_2913_web/logout_Servlet" method="post">
        <input class="button" type="submit" value="Logout">
    </form>
</div>
</body>
<script>
    function myFunction() {
        document.getElementsByClassName("downMenu")[0].classList.toggle("responsive");
    }
</script>

</html>
