<%@ page language="java" contentType="text/html; charset=GB18030" pageEncoding="GB18030"%>
<html>

<head>
	<meta charset="utf-8">
	<title>rent</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="../css/select_car.min.css" />
	<link rel="stylesheet" href="../css/select_car.css">
	<style>
		body {
			padding: 0;
			margin: 0;
		}

		.main {
			padding-top: 50px;
			padding-bottom: 60px;
			min-width: 375px;
		}

		/* herder bar */
		.header {
			width: 100vw;
			background-color: #1C1C1C;
			display: flex;
			position: fixed;
			top: 0;
			left: 0;
			justify-content: space-between;
			align-items: center;
			font-size: 1vw;
		}

		/* herder jump */
		.header .left {
			margin-left: 15px;
			display: flex;
			align-items: center;
			justify-content: space-between;
			overflow: hidden;
		}

		.header a {
			color: white;
			text-align: center;
			text-decoration: none;
			margin-left: 2vw;
		}

		.header a:hover {
			background-color: #ddd;
			color: black;
		}

		/* herder login */
		.login {
			margin-right: 25px;
		}

		/* size of background */
		.bgc {
			background-color: white;
			margin-left: 15%;
			width: 70%;
			border: 1px solid #eee;
			box-shadow: 0 2px 10px 0 rgb(0 0 0 / 10%);
			overflow: hidden;
		}

		/* choose bar */
		.column {
		    margin-left: 5%;
			background-color: #FFFFFF;
			font-size: 1.5vw;

		}

		.column form {
			width: 100%;
			display: flex;
			justify-content: space-around;
			flex-wrap: wrap;
		}


		.hr {
			width: 100%;
			border: 0;
			height: 1px;
			background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
		}

		.middle {
			font-size: 1.5vw;
		}

		.middle div img {
			width: 50%;
		}

		.middle h2 {
			font-size: 1.8vw;
		}

		.middle h3 {
			font-size: 1.5vw;
		}

		.middle p {
			font-size: 1.2vw;
		}

		.middle-main {
			display: flex;
			justify-content: space-around;
			flex-wrap: wrap;
		}

		.middle-main>div {
			width: 39%;
			min-width: 200px;
			margin: 10px;
			padding: 10px;
			box-shadow: 2px 2px 5px #000;
			background-color: #FFFFFF;
		}

		.button {
			margin-left: 10px;
		}

		.button {
			width: 70px;
			height: 30px;
			line-height: 25px;
			background-color: rgb(0, 0, 0);
			border: none;
			color: #fff;
			border-radius: 3px;
		}
	</style>
</head>

<body>
<%@ include file="template/top.jsp"%>
	<div class="main">
		<div class="container">
			<div class="bgc">
				<div class="row">
					<!-- choose data -->
					<h3></h3>
					<form action="select_id.jsp" method="post">
						<div class="column">
							<h3>Select duration:</h3>
							<a ><input name="Time" type="radio" value="1" checked/>1 hour&emsp;</a>
							<a ><input name="Time" type="radio" value="4" />4 hours&emsp;</a>
							<a ><input name="Time" type="radio" value="24" />1 day&emsp;</a>
							<a ><input name="Time" type="radio" value="168" />1 week</a>
						</div>

					<!-- Dividing Line -->
					<hr class="hr">
					<!-- main data -->
					<div class="middle">
						<h2 style="text-align:center;" >Station:</h2>

						<div class="middle-main">
							<div class="morrison">
								<h3>Merrion centre</h3>
								<p>(Merrion Shopping Centre,LS2 8PL)</p>
								<div class="tempStyle">
									<input class="hy-radio" type="radio" checked="true" value="Merrion_centre" name="location"><label>choose this station</label>
								</div>
								<div class="img1">
									<img src="../image/morrison.png" width="350" height="200">
								</div>
							</div>
							<div class="train">
								<h3>Train station</h3>
								<p>(New Station St,Leeds LS1 4DY)</p>
								<div class="tempStyle">
									<input class="hy-radio" type="radio" value="Train_station" name="location"><label>choose this station</label>
								</div>
								<div class="img2">
									<img src="../image/train.png" width="350" height="200">
								</div>
							</div>
							<div class="trinity">
								<h3>Trinity centre</h3>
								<p>(27 Albion St,Leeds LS1 5AT)</p>
								<div class="tempStyle">
									<input class="hy-radio" type="radio" value="Trinity_centre" name="location"><label>choose this station</label>
								</div>
								<div class="img3">
									<img src="../image/trinity.png" width="350" height="200">
								</div>
							</div>
							<div class="uol">
								<h3>UoLEdge sports centre</h3>
								<p>(Willow Terrace Road,Leeds LS2 9JT)</p>
								<div class="tempStyle">
									<input class="hy-radio" type="radio" value="UoLEdge_sports_centre" name="location"><label>choose this station</label>
								</div>
								<div class="img4">
									<img src="../image/edge.png" width="350" height="200">
								</div>
							</div>
							<div class="lri">
								<h3>LRl hospital</h3>
								<p>(Great George St,Leeds LS1 3EX)</p>
								<div class="tempStyle">
									<input class="hy-radio" type="radio" value="LRl_hospital" name="location"><label>choose this station</label>
								</div>
								<div class="img5">
									<img src="../image/hospital.png" width="350" height="200">
								</div>
							</div>
						</div>

						<!-- submit button -->
					 	<input class="button" type="submit" value="Confirm" style="margin-left: 20px;">
					</div>
					</form>

				</div>
			</div>
		</div>

	</div>
	<!-- header -->
<%@ include file="template/footer.jsp"%>
	<script src="../js/rent.min.js"></script>
</body>

</html>