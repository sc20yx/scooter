<%@ page import="comp2913.web.Dao_for_mysql" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Connection" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../css/sz_boot.css">
    <link rel="stylesheet" href="../../css/base.css">
    <link rel="stylesheet" href="../../css/order_base.css">
    <link rel="stylesheet" href="../../css/order_cancel.css">
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="../../js/sz_boot.js"></script>
    <title>Order Cancel</title>
</head>
<body>
<%@ include file="../template/stuff_top.jsp"%>
<div class="container">
    <!-- Navigation end -->
    <div class="main">
        <div class="row">
            <div class="col-md-7">
                <!-- Order list page start -->
                <%
                    Connection conn = Dao_for_mysql.getConn();
                    ResultSet b = Dao_for_mysql.getAll(conn,"select * from stuff_order where stuff_order_pay = 0 and stuff_order_confirm = 0;");

                    List<Object[]> order_user = new ArrayList<Object[]>();
                    try {
                        if (b != null) {
                            while (b.next()) {
                                String order_id = b.getString("stuff_order_id");
                                String cU_id = b.getString("customer_name");
                                String scooter_id = b.getString("scooter_id");
                                Date order_time = b.getDate("stuff_order_time");
                                int order_duration = b.getInt("stuff_order_duration");
                                int order_money = b.getInt("stuff_order_money");
                                int comfirm = b.getInt("stuff_order_confirm");
                                order_user.add(new Object[]{order_id, scooter_id, order_time, order_duration, order_money, cU_id,comfirm,0});
                            }
                            b.close();
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    if(order_user!= null){
                        for(int i = 0; i < order_user.size();i++){

                %>
                <div class="order-area card">
                    <div class="card-header bg-warning">
                        <div class="row">
                            <div class="col-md-4">Order Number:<%= order_user.get(i)[0]%></div>
                            <div class="col-md-4">
                                <form action="/java_2913_web/cancel_appointment_Servlet" method="post">
                                    <input type="hidden" value="<%= order_user.get(i)[0]%>" name="order_id">
                                    <input type="hidden" value="<%= order_user.get(i)[7]%>" name="type">

                                    <input type="submit" class="btn btn-danger btn-sm" value="Cancel">
                                </form>
                            </div>
                            <%
                                if((int)order_user.get(i)[6] == 1){
                            %>
                            <div class="col-md-4 status-area">Status: <span class="text-info">Not Confirm</span></div>
                            <%
                                }
                                if((int)order_user.get(i)[6] == 0){
                            %>
                            <div class="col-md-4 status-area">Status: <span class="text-info">Confirmed</span></div>
                            <%
                                }
                            %>
                        </div>
                    </div>
                    <div class="card-body text-left bg-light">
                        <div class="row">

                            <div class="col-md-4 data-area">Duration:<%= order_user.get(i)[3]%> </div>
                            <div class="col-md-4 data-area">Money:<%= order_user.get(i)[4]%> </div>
                            <div class="col-md-4 data-area">Scooter id:<%= order_user.get(i)[1]%> </div>
                            <br>
                            <div class="col-md-4 data-area">
                                <%
                                    if((int)order_user.get(i)[6] == 0){
                                %>
                                <form action="/java_2913_web/pay_Servlet" method="post">
                                    <input type="hidden" value="<%= order_user.get(i)[0]%>" name="order_id">
                                    <input type="hidden" value="<%= order_user.get(i)[7]%>" name="type">
                                    <input type="submit" class="btn btn-primary btn-lg" value="Pay">
                                </form>
                                <%
                                    }
                                %>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 data-area"></div>
                            <div class="col-md-4 data-area">Customer id:<%=order_user.get(i)[5]%> </div>
                            <div class="col-md-4 data-area"><span class="bg-success money-area">Date time:<%= order_user.get(i)[2]%> </span></div>
                        </div>
                    </div>
                </div>
                <%
                    }
                }else{
                %>
                <div class="order-area card">
                    <div class="card-header bg-warning">
                        <div class="row">
                            <div class="col-md-4">Order Number:</div>
                            <div class="col-md-4">
                                <form action="/java_2913_web/cancel_appointment_Servlet" method="post">
                                    <input type="hidden" value="" name="order_id">
                                    <input type="button" class="btn btn-danger btn-sm" value="Cancel">
                                </form>
                            </div>
                            <div class="col-md-4 status-area">Status: <span class="text-info">none</span></div>
                        </div>
                    </div>
                    <div class="card-body text-left bg-light">
                        <div class="row">
                            <div class="col-md-4 data-area">Duration: </div>
                            <div class="col-md-4 data-area">Money: </div>
                            <div class="col-md-4 data-area">Scooter id: </div>
                            <br>
                            <div class="col-md-4 data-area">
                                <form action="/java_2913_web/book_confirm_servlet" method="post">
                                    <input type="hidden" value="" name="order_id">
                                    <input type="button" class="btn btn-primary btn-lg" value="Confirm">
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 data-area"></div>
                            <div class="col-md-4 data-area">Customer id: </div>
                            <div class="col-md-4 data-area"><span class="bg-success money-area">Date time: </span></div>
                        </div>
                    </div>
                </div>
                <%
                    }
                %>
                <!-- Order list page end -->
            </div>
            <div class="col-md-5">
                <!-- Order confirm start -->
                <div class="order-confirm-area text-center"  style="background-color: #ffffff">
                    <h5><a href="./order_cancel.jsp">Order Confirm</a></h5>
                </div>
                <!-- Order confirm end -->
                <div class="img-area">
                    <img src="../../image/logo.png" alt="" class="img-fluid ">
                </div>
            </div>
        </div>

    </div>
</div>


</body>
</html>

