<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../../css/global.css">
    <link rel="stylesheet" href="../../css/style.css">
</head>
<body>
<%@ include file="../template/stuff_top.jsp"%>
<%
    String idid = (String)session.getAttribute("previo_id");
    String emem = (String)session.getAttribute("previo_email");
    if(idid == null){
        idid = "";
    }
    if(emem == null){
        emem = "";
    }
    session.removeAttribute("previo_id");
    session.removeAttribute("previo_email");
%>
<!--end-->

<!-- information -->
<section class="content2">
    <div class="form">
        <label>Users:</label>
        <div class="form-body">
              <form action="/java_2913_web/search_orderByid_Servlet" method="post">
                <input id="E-mail" type="text" value="<%=emem%>" name="oooo" placeholder="E-Mail address">
                <input id="O-Number" type="text" value="<%=idid%>" name="order_ida" placeholder="Order Number">
                <div class="search">
                    <input type="submit" value="search">
                </div>
              </form>
        </div>
    </div>

    <%
        //haven't search; no information
        Object[] b = (Object[]) session.getAttribute("email_order");
        session.removeAttribute("email_order");
        if(b == null){
    %>
    <div class="form">
        <label>Order Information:</label>
        <div class="form-body bg-form">
            <div class="form-row">
                <div class="row-cell">ID</div>
                <div class="row-cell">00000000</div>
                <div class="row-cell">Time</div>
                <div class="row-cell">0 hour</div>
            </div>
            <div class="form-row">
                <div class="row-cell">Scooter ID</div>
                <div class="row-cell">000</div>
                <div class="row-cell">Start Time</div>
                <div class="row-cell">00:00</div>
            </div>
            <div class="form-row">
                <div class="row-cell">Fee</div>
                <div class="row-cell">£0</div>
                <div class="row-cell"></div>
                <div class="row-cell"></div>
            </div>
        </div>
    </div>


    <%
        }
        // Information retrieved and displayed
        if(b != null){

    %>
    <div class="form">
        <label>Order Information:</label>
        <div class="form-body bg-form">
            <div class="form-row">
                <div class="row-cell">ID</div>
                <div class="row-cell"><%=b[0]%></div>
                <div class="row-cell">Time</div>
                <div class="row-cell"><%=b[1]%></div>
            </div>
            <div class="form-row">
                <div class="row-cell">Scooter ID</div>
                <div class="row-cell"><%=b[2]%></div>
                <div class="row-cell">Start Time</div>
                <div class="row-cell"><%=b[3]%></div>
            </div>
            <div class="form-row">
                <div class="row-cell">Fee</div>
                <div class="row-cell"><%=b[4]%></div>
                <div class="row-cell"></div>
                <div class="row-cell"></div>
            </div>
        </div>
    </div>
    <%
      }
    %>
</section>
<!-- information end -->

<!-- remark -->
    <div >
        <form id="send_email" action="/java_2913_web/send_massage_Servlet" method="post">
            <section class="remark-center" style="margin: 0 auto;">
                <div class="form">
                    <label>Remark:</label>
                    <div class="form-body">
                        <input type="hidden" id="emaila" name="email_ida" >
                        <input type="hidden" id="order_id" name="order_ida" >
                        <textarea name="content_email" cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div class="send">
                    <input type="button" onclick="x();" value="send">
                </div>
            </section>
        </form>
    </div>


<!-- remark end -->


<script>
    function x() {
        var a = document.getElementById("E-mail").value;
        var orde = document.getElementById("O-Number").value;
        if (a.length == 0 || orde.length == 0) {
            window.alert("your email address or order id is empty");
        } else {
            document.getElementById("emaila").value = a;
            document.getElementById("order_id").value = orde;
            document.getElementById("send_email").submit();
        }
    }
</script>
</body>
</html>
