<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../css/bootstrap.min_feedback.css">
    <link rel="stylesheet" href="../../css/base.css">
    <link rel="stylesheet" href="../../css/form.css">
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="../../js/bootstrap.min_feedback.js"></script>
    <title>Feedback</title>
</head>
<body>
<%@ include file="../template/top.jsp"%>
    <div class="container">
        <!-- Navigation end -->
        <div class="main">
            <!-- form start -->
            <div class="form-area">
                <form action="/java_2913_web/send_Feedback_Servlet" method="post">
                    <div class="form-group row">
                          <label for="problem_type" class="col-md-3 col-form-label">Types of problems: </label>
                          <div class="col-md-7">
                            <select name="problem_type" id="problem_type" class="select form-control">
                                <option value="Order">Order</option>
                                <option value="Payment">Payment</option>
                                <option value="Scooter">Scooter</option>
                                <option value="Login">Login</option>
                                <option value="Service">Service</option>
                                <option value="Other">Other</option>
                            </select>
                          </div>
                    </div>
                    <div class="form-group row">
                        <label for="problem_detail" class="col-md-3 col-form-label">Detailed problems: </label>
                        <div class="col-md-7" id="problem_detail_area">
                          <select name="problem_detail" id="problem_detail" class="select form-control">
                              <option value="Buy a scooter">Buy a scooter</option>
                              <option value="Electricity for scooters">Electricity for scooters</option>
                              <option value="Others">Others</option>
                          </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="problem_detail" class="col-md-3 col-form-label">Problem Description: </label>
                        <div class="col-md-7">
                              <textarea name="message_content" id="" cols="20" rows="5" class="textarea form-control">

                              </textarea>
                        </div>
                    </div>
                    <div class="btn-area row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 text-center" >
                            <input type="submit" value="submit" class="btn feedback_btn" style="color:#FFFFFF; background-color: #0a0a0a ">
                        </div> 
                    </div>
                </form>
            </div>
            <!-- form end -->
        </div>
    </div>

    <script>
        $(function(){
            $('select#problem_type').change(function(){
                /**
                 * Dynamic updates select
                 */
                var problem = $(this).val();
                console.log(problem);
                var html = gen_select(problem_level_data_list, problem);
                $('div#problem_detail_area').html(html);
            });
        });
        var problem_level_data_list = {
            // List of problem levels
            'Order': ['Order disappeared', 'Doubts about the order', 'Order status', 'Other'],
            'Payment': ['Time of use issues', 'Unable to pay problem', 'Payment amount issues', 'Other'],
            'Scooter': ['Book a scooter', 'Scooter power', 'Scooter breakdown', 'Other'],
            'Login': ['Change of password', 'Unable to login', 'change of personal information', 'Other'],
            'Service': ['service issues for stuff', 'Scooter experience', 'Experience using the websites', 'Other'],
            'Other': ['Other', ]
        }

        function gen_select(problem_level_data_list, problem) {
            var options = problem_level_data_list[problem]; // get ProblemDetail
            console.log(options);
            // Generate select
            var html = '<select name="problem_detail" id="problem_type" class="select form-control">';
            for (let index = 0; index < options.length; index++) {
                const element = options[index];
                html += '<option value="' + element + '" >' + element + '</option>';
            }
            html += '</select>'
            return html
        }
    </script>
</body>
</html>
