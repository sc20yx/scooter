<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="comp2913.web.Dao_for_mysql" %>
<%@ page import="java.util.List" %>
<%@ page import="java.sql.Date" %>
<%@ page import="java.sql.Connection" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../css/sz_boot.css">
    <link rel="stylesheet" href="../../css/base.css">
    <link rel="stylesheet" href="../../css/order.css">
    <link rel="stylesheet" href="../../css/order1.css">
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="../../js/sz_boot.js"></script>
    <title>Order-Not-Start</title>
</head>
<body>
<%@ include file="../template/top.jsp"%>
    <div class="container">
        <!-- Navigation end -->
        <div class="main">
            <div class="row">
                <!-- Left menu start -->
                <div class="col-md-3 menu_area text-center">
                    <ul class="list-group">
                        <li class="list-group-item menu_active">
                            <a href="./order_not_start.jsp">Not comfirmed</a>
                        </li>
                        <li class="list-group-item">
                            <a href="./order_underway.jsp">comfirmed</a>
                        </li>
                        <li class="list-group-item">
                            <a href="./order_completed.jsp">Completed</a>
                        </li>
                    </ul>
                </div>
                <!-- Left menu end -->
                <!-- Order list start -->
                <div class="col-md-9">
                    <%
                        Connection conn = Dao_for_mysql.getConn();
                        int email = (int)request.getSession().getAttribute("user__id");

                        ResultSet a = Dao_for_mysql.getAll(conn,"select * from `order` where customer_id = " + email +" and order_confirm = 1;");
                        List<Object[]> order_user = new ArrayList<Object[]>();
                        try {
                            if(a!=null) {
                                while (a.next()) {
                                    String order_id = a.getString("order_id");
                                    String scooter_id = a.getString("scooter_id");
                                    Date order_time = a.getDate("order_time");
                                    int order_duration = a.getInt("order_duration");
                                    int order_money = a.getInt("order_money");

                                    order_user.add(new Object[]{order_id, scooter_id, order_time, order_duration, order_money});
                                }
                                a.close();
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        if(order_user!= null){
                            for(int i = 0; i < order_user.size();i++){
                    %>
                        <div class="order-area card">
                            <div class="card-header bg-warning">
                                <div class="row">
                                    <div class="col-md-4">Order Number: <%= order_user.get(i)[0]%></div>
                                    <div class="col-md-4 status-area"><span class="text-info"></span></div>
                                    <div class="col-md-4">Date time:<%= order_user.get(i)[2]%> </div>
                                    <div class="col-md-4">Scooter id:<%= order_user.get(i)[1]%> </div>
                                </div>
                            </div>
                            <div class="card-body text-left bg-light">
                                <div class="row">
                                    <form action="/java_2913_web/longer_appointment_Servlet" method="post">
                                        <input type="hidden" name="order_id" value="<%=order_user.get(i)[0]%>">
                                        <div class="col-md-4 data-area">
                                            <input type="submit" value="Longer" class="btn btn-danger">
                                        </div>
                                    </form>
                                    <div class="col-md-4 data-area">Duration:<%=order_user.get(i)[3]%></div>
                                    <div class="col-md-4 data-area">Money:<%=order_user.get(i)[4]%></div>
                                    <form action="/java_2913_web/cancel_appointment_Servlet" method="post">
                                        <input type="hidden" name="order_id" value="<%=order_user.get(i)[0]%>">
                                        <input type="hidden" value="1" name="type">
                                        <div class="col-md-4 data-area">
                                            <input type="submit" value="Cancel" class="btn btn-danger">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    <%
                            }
                        }else{
                    %>
                    <div class="order-area card">
                        <div class="card-header bg-warning">
                            <div class="row">
                                <div class="col-md-4">Order Number: </div>
                                <div class="col-md-4 status-area"><span class="text-info"></span></div>
                                <div class="col-md-4">Date time: </div>
                                <div class="col-md-4">Scooter id: </div>
                            </div>
                        </div>
                        <div class="card-body text-left bg-light">
                            <div class="row">
                                <div class="col-md-4 data-area">
                                    <input type="button" value="Longer"  class="btn btn-danger">
                                </div>
                                <div class="col-md-4 data-area">Duration:</div>
                                <div class="col-md-4 data-area">Money:</div>
                                <div class="col-md-4 data-area">
                                    <input type="button" value="Cancel" class="btn btn-danger">
                                </div>
                            </div>
                        </div>
                    </div>
                    <%
                        }
                    %>
                </div>
                <!-- Order list end -->
            </div>
        </div>
    </div>
</body>
</html>
