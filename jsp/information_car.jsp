<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html lang="en">
<meta http-equiv="Content-Type" Content="text/html;charset=utf8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
<link rel="stylesheet" href="../css/select_car.min.css" />
<link rel="stylesheet" href="../css/select_car.css">

<head>
    <title>cep</title>
  <style>
        .pay-btn {
            display: inline-block;
            height: 48px;
            width: 80%;
            line-height: 48px;
            color: white;
            background-color: #eb5b00;
            text-decoration: none;
            text-align: center;
            border-radius: 12px;
        }

        .pay-btn:hover {
            text-decoration: none
        }

        .date-widget{
            width: 80%;
            height: 48px;
            line-height: 48px;
            font-size: 18px;
            margin-bottom: 12px;
            font-weight: bold; cursor: pointer;padding: 0 20px;border-radius: 12px;
        }
        .buy-btn{
            cursor: pointer;
            position: absolute;
            height: 48px;
            width: 48px;
            border-radius: 50%;
            background-color: coral;
            text-align: center;
            line-height: 48px;
            font-size: 24px;
            color: white;
            left: 15px;
            top: 15px;
            z-index: 999;
            border: 1px solid #eee;
            box-shadow: 0 2px 10px 0 rgb(0 0 0 / 10%);
        }
        .buycar-panel{
            height:500px;
            width:500px;
            background-color: #fff;
            border: 1px solid #eee;
            position: relative;
            box-shadow: 0 2px 10px 0 rgb(0 0 0 / 10%);
            margin-top: 40px;
            margin-left:330px;
            padding: 15px;
            box-shadow: 10px 10px 5px #888888;
        }
        .ID{
         border:0px;
		 outline:none;
         }
         .price{
         width:60px;
         }
         .button{
         margin-top:50px;
         background-color:#F7BE4A;
         border-radius:10%;
         outline:none;
         border:none;
         color:white;
         padding:5px 10px;
         }
		 
		 /*iphone style*/
		 @media (min-width: 400px) and (max-width:767px) {
           .container div:nth-child(1){
             margin-left: unset !important;
             width:100% !important;
           }
           .buycar-panel{
             margin-left:unset !important;
             box-shadow: unset !important;
           }
           .save_btn{
             margin-top:30px !important;
           }

		 }
       </style>
</head>
<%
 Character a = (Character)session.getAttribute("account_type");
%>
<body>
<%@ include file="template/trends_top.jsp"%>
<%
    String id_scoot = (String)request.getParameter("id");
    String reser_scoot = (String)request.getParameter("reservable");
    String sta_scoot = (String)request.getParameter("car_avalib");
    String loca_scoot = (String)request.getParameter("location_scooter");
    String pricea_scoot = (String)request.getParameter("scoo_price");
%>
<div class="container" style="width:100%">
    <div style="
        margin:0 auto;
        position: relative;
        width:1100px;
        height:600px;">

        <%
                        if(a.equals('1')){
                            //admin
                    %>
    <form action="/java_2913_web/scooter_update_Servlet" method="post">
        <div class="buycar-panel" style=" box-shadow: 10px 10px 5px #888888;">
        <br>
        <br>
        <p>Scooter ID:<input type="text" name="id_scoot" readonly="readonly" value="<%=id_scoot%>" class="ID"></p>
        <br>
            <div class="rentable">
            <p>Rentable:</p>
            <select class="select form-control" name="reserve_scoot">
            <%
                if(reser_scoot.equals("0")){
            %>
            <option value="0" selected="selected">available</option>
            <option value="1">unavailable</option>
            <%
                }else{
                if(reser_scoot.equals("1")){
            %>
            <option value="0" >available</option>
            <option value="1" selected="selected">unavailable</option>
            <%
                }
                else{
            %>
            <option value="0" >available</option>
            <option value="1" selected="selected">unavailable</option>
            <%
                    }
                }
                %>
                </select>
            </div>
            <br>
<div class="status">
            <p>Status:</p>
            <select class="select form-control" name="status_scoot">
            <%
               if(sta_scoot.equals("0")){
            %>
            <option value="0" selected="selected">available</option>
            <option value="1">unavailable</option>
            <option value="2">broken</option>
            <%
               }else{
               if(sta_scoot.equals("1")){
            %>
            <option value="0">available</option>
            <option value="1"selected="selected">unavailable</option>
            <option value="2">broken</option>
            <%
               }else{if(sta_scoot.equals("2")){
            %>
            <option value="0">available</option>
            <option value="1">unavailable</option>
            <option value="2"selected="selected">broken</option>
            <%
               }else{
            %>
            <option value="0">available</option>
            <option value="1" selected="selected">unavailable</option>
            <option value="2">broken</option>
            <%
               }}}
            %>
               </select>
            </div>
            <br>
            <div class="location">
            <p>Location:</p>
            <select class="select form-control" name="position_scoot">
            <%
                if(loca_scoot.equals("Trinity_centre")){
            %>
            <option value="Trinity_centre" selected="selected">Trinity centre</option>
            <option value="LRl_hospital">LRl hospital</option>
            <option value="Train_station">Train station</option>
            <option value="UoLEdge_sports_centre">UoLEdge sports centre</option>
            <option value="Merrion_centre">Merrion centre</option>
            <option value="none">none</option>
            <%
                }else{ if(loca_scoot.equals("LRl_hospital")){
            %>
            <option value="Trinity_centre">Trinity centre</option>
            <option value="LRl_hospital"selected="selected">LRl hospital</option>
            <option value="Train_station">Train station</option>
            <option value="UoLEdge_sports_centre">UoLEdge sports centre</option>
            <option value="Merrion_centre">Merrion centre</option>
            <option value="none">none</option>
            <%
                }else{if(loca_scoot.equals("Train_station")){
            %>
            <option value="Trinity_centre">Trinity centre</option>
            <option value="LRl_hospital">LRl hospital</option>
            <option value="Train_station"selected="selected">Train station</option>
            <option value="UoLEdge_sports_centre">UoLEdge sports centre</option>
            <option value="Merrion_centre">Merrion centre</option>
            <option value="none">none</option>
            <%
                }else{if(loca_scoot.equals("UoLEdge_sports_centre")){
            %>
            <option value="Trinity_centre">Trinity centre</option>
            <option value="LRl_hospital">LRl hospital</option>
            <option value="Train_station">Train station</option>
            <option value="UoLEdge_sports centre"selected="selected">UoLEdge sports centre</option>
            <option value="Merrion_centre">Merrion centre</option>
            <option value="none">none</option>
            <%
                }else{if(loca_scoot.equals("Merrion_centre")){
            %>
            <option value="Trinity_centre">Trinity centre</option>
            <option value="LRl_hospital">LRl hospital</option>
            <option value="Train_station">Train station</option>
            <option value="UoLEdge_sports centre">UoLEdge sports centre</option>
            <option value="Merrion_centre"selected="selected">Merrion centre</option>
            <option value="none">none</option>
            <%
                }else{
            %>
            <option value="Trinity_centre">Trinity centre</option>
            <option value="LRl_hospital">LRl hospital</option>
            <option value="Train_station">Train station</option>
            <option value="UoLEdge_sports_centre">UoLEdge sports centre</option>
            <option value="Merrion_centre">Merrion centre</option>
            <option value="none"selected="selected">none</option>
            <%
                }}}}}
            %>
                </select>
            </div>
            <br>
            <br>
                <p>Price:<input type="text" name="price_scoot"value="<%=pricea_scoot%>" oninput="value=value.replace(/[^\d]/g,'')" class="price"></p>

                <div style="text-align:right;">
                    <button type="submit" class="button">Save</button>
                </div>
            </div>
        </form>
            <%
                }
                if(a.equals('2')){
                    //staff
            %>
        <form id="form_bb" action="/java_2913_web/scooter_update_Servlet" method="post">
            <div class="buycar-panel" style=" box-shadow: 10px 10px 5px #888888;">
                <br>
                <br>
                <p>Scooter ID:<input type="text" name="id_scoot" readonly="readonly" value="<%=id_scoot%>" class="ID"></p>
                <br>
                <div class="rentable">
                    <p>Rentable:</p>
                    <select id="sel2" class="select form-control" disabled="disabled" name="reserve_scoot">
                        <%
                            if(reser_scoot.equals("0")){
                        %>
                        <option value="0" selected="selected">available</option>
                        <option value="1">unavailable</option>
                        <%
                        }else{
                            if(reser_scoot.equals("1")){
                        %>
                        <option value="0" >available</option>
                        <option value="1" selected="selected">unavailable</option>
                        <%
                        }
                        else{
                        %>
                        <option value="0" >available</option>
                        <option value="1" selected="selected">unavailable</option>
                        <%
                                }
                            }
                        %>
                    </select>
                </div>
                <br>
                <div class="status">
                    <p>Status:</p>
                    <select class="select form-control" name="status_scoot">
                        <%
                            if(sta_scoot.equals("0")){
                        %>
                        <option value="0" selected="selected">available</option>
                        <option value="1">unavailable</option>
                        <option value="2">broken</option>
                        <%
                        }else{
                            if(sta_scoot.equals("1")){
                        %>
                        <option value="0">available</option>
                        <option value="1"selected="selected">unavailable</option>
                        <option value="2">broken</option>
                        <%
                        }else{if(sta_scoot.equals("2")){
                        %>
                        <option value="0">available</option>
                        <option value="1">unavailable</option>
                        <option value="2"selected="selected">broken</option>
                        <%
                        }else{
                        %>
                        <option value="0">available</option>
                        <option value="1" selected="selected">unavailable</option>
                        <option value="2">broken</option>
                        <%
                                    }}}
                        %>
                    </select>
                </div>
                <br>
                <div class="location">
                    <p>Location:</p>
                    <select id="sel1" class="select form-control" disabled="disabled" name="position_scoot">
                        <%
                            if(loca_scoot.equals("Trinity_centre")){
                        %>
                        <option value="Trinity_centre" selected="selected">Trinity centre</option>
                        <option value="LRl_hospital">LRl hospital</option>
                        <option value="Train_station">Train station</option>
                        <option value="UoLEdge_sports_centre">UoLEdge sports centre</option>
                        <option value="Merrion_centre">Merrion centre</option>
                        <option value="none">none</option>
                        <%
                        }else{ if(loca_scoot.equals("LRl_hospital")){
                        %>
                        <option value="Trinity_centre">Trinity centre</option>
                        <option value="LRl_hospital"selected="selected">LRl hospital</option>
                        <option value="Train_station">Train station</option>
                        <option value="UoLEdge_sports_centre">UoLEdge sports centre</option>
                        <option value="Merrion_centre">Merrion centre</option>
                        <option value="none">none</option>
                        <%
                        }else{if(loca_scoot.equals("Train_station")){
                        %>
                        <option value="Trinity_centre">Trinity centre</option>
                        <option value="LRl_hospital">LRl hospital</option>
                        <option value="Train_station"selected="selected">Train station</option>
                        <option value="UoLEdge_sports_centre">UoLEdge sports centre</option>
                        <option value="Merrion_centre">Merrion centre</option>
                        <option value="none">none</option>
                        <%
                        }else{if(loca_scoot.equals("UoLEdge_sports_centre")){
                        %>
                        <option value="Trinity_centre">Trinity centre</option>
                        <option value="LRl_hospital">LRl hospital</option>
                        <option value="Train_station">Train station</option>
                        <option value="UoLEdge_sports centre"selected="selected">UoLEdge sports centre</option>
                        <option value="Merrion_centre">Merrion centre</option>
                        <option value="none">none</option>
                        <%
                        }else{if(loca_scoot.equals("Merrion_centre")){
                        %>
                        <option value="Trinity_centre">Trinity centre</option>
                        <option value="LRl_hospital">LRl hospital</option>
                        <option value="Train_station">Train station</option>
                        <option value="UoLEdge_sports centre">UoLEdge sports centre</option>
                        <option value="Merrion_centre"selected="selected">Merrion centre</option>
                        <option value="none">none</option>
                        <%
                        }else{
                        %>
                        <option value="Trinity_centre">Trinity centre</option>
                        <option value="LRl_hospital">LRl hospital</option>
                        <option value="Train_station">Train station</option>
                        <option value="UoLEdge_sports_centre">UoLEdge sports centre</option>
                        <option value="Merrion_centre">Merrion centre</option>
                        <option value="none"selected="selected">none</option>
                        <%
                                            }
                                    }
                                    }
                                    }
                                        }
                        %>
                    </select>
                </div>
                <br>
                <br>
                <p>Price:<input type="text" readonly="readonly" name="price_scoot"value="<%=pricea_scoot%>" oninput="value=value.replace(/[^\d]/g,'')" class="price"></p>

            <div style="text-align:right;">
                                <input type="button" class="button"onclick="chan();" value="Save">
                            </div>
                        </div>
                    </form>
                        <%
                            }
                        %>
                </div>
            </div>
<%@ include file="template/footer.jsp"%>
<script>
    function chan() {
        document.getElementById("sel1").removeAttribute("disabled");
        document.getElementById("sel2").removeAttribute("disabled");
        document.getElementById("form_bb").submit();
    }
</script>
</body>
</html>