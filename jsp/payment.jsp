<%@ page import="comp2913.web.Dao_for_mysql" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Connection" %>
<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>payment</title>
    <link rel="stylesheet" href="../css/payment.css" type="text/css">
</head>

<body>
<%@ include file="template/top.jsp"%>
    <div class="w">
        <div class="content">
<%--            get the card information --%>
            <div class="left_box">
                <ul>
                    <%
                        Connection conn = Dao_for_mysql.getConn();
                        int id_user = (int)request.getSession().getAttribute("user__id");

                        ResultSet a = Dao_for_mysql.getAll(conn,"select * from card where customer_id = "+id_user+";");
                        List<Object[]> cards = new ArrayList<Object[]>();
                        try {
                            if(a != null) {
                                while (a.next()) {
                                    String card_num = a.getString("card_number");
                                    String card_na = a.getString("card_name");
                                    cards.add(new Object[]{card_na,card_num});
                                }
                            }
                            a.close();
                            request.getSession().setAttribute("cards_list",cards);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        conn.close();
                        if(a == null){

                        }else{
                            for(int i = 0;i< cards.size();i++){
                    %>
<%--                    show card information--%>
                    <input type="checkbox" name="payN" style="width: 59px;height: 44px;border-radius: 50%; background-color: #80808099;" />

                    <li class="first">
                        <form action="/java_2913_web/delete_card_Servlet" method="post">
                            <div class="info">
                                <input  class="card" type="text" name="cards_name" value="<%=cards.get(i)[0]%>" readonly="readonly">
                                <br>
                                <br>
                                <input class="card" type="text" name="cards_number" value="<%=cards.get(i)[1]%>" readonly="readonly">
                                <br>
                                <button type="button" class="edit" ><a href="wallet2.jsp">Edit</a></button>
                                <input type="submit" class="edit" value="Delete">
                            </div>
                        </form>
                    </li>
                    <%
                            }
                        }
                    %>
                    <li class="second">
                        <div class="info">
                            <a href="wallet.jsp" target="_blank">Add credit</a>
                        </div>
                    </li>
                </ul>
            </div>
            <%
                String orderNum = (String) request.getParameter("order_number_a");
                String money = (String)request.getParameter("moneya");
            %>
<%--    the function for payment--%>
            <div class="right_box">
                <form action="/java_2913_web/pay_Servlet" id="paynow" method="post">
                    <h3>Bill</h3>
                    <div class="amount">
                        <h4>Amount</h4>
                        <input type="hidden" value="<%= orderNum%>" name="order_id">
                        <input class="bill" readonly="readonly" type="text" value="<%=money%>">
                        <p class="amount-num"></p>
                    </div>
                    <div class="order_num">
                        <h4>Order number</h4>
                        <input class="bill" readonly="readonly" name="order_payed" type="text" value="<%=orderNum%>">
                        <p></p>
                    </div>
                    <input type="button" onclick="x()" value="Paynow">
                </form>
            </div>
        </div>
    </div>
</body>
<script>
    function myFunction() {
        document.getElementsByClassName("downMenu")[0].classList.toggle("responsive");
    }

    function x() {
        var list = document.getElementsByName("payN");
        var str = "";
        for(var i = 0; i < list.length;i++){
            if (list[i].checked) {
                str = "have card";
            }
        }
        if (str.length == 0) {
            window.alert("please select card");
        } else {
            document.getElementById("paynow").submit();
        }


    }
</script>
</html>
