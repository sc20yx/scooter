<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="always" name="referrer">
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>signUp</title>
    <link rel="stylesheet" href="../css/reset.css" type="text/css" />
    <link rel="stylesheet" href="../css/style1.css" type="text/css" />
    <link rel="stylesheet" href="../css/response.css" type="text/css" />

</head>

<body >
<%@ include file="template/top.jsp"%>

    <!--wrapper -->
    <div id="wrapper">
        <!--header-->
        <!--contentbox  -->
        <div class="content mar">
            <form id="regi" action="/java_2913_web/register_Servlet" method="post">
            <div class="con_list">
                <h1 class="mar">sign up</h1>
                <ul class="mar">
                    <li>
                        <span class="fl">username:</span>
                        <input type="text" id="va1" name="customer_name" class="fl" />
                    </li>
                    <li>
                        <span class="fl">useacount:</span>
                        <input type="text" id="va2" name="customer_account" class="fl" />
                    </li>
                    <li>
                        <span class="fl">license:</span>
                        <input type="text"id="va3" name="customer_licence" maxlength="16" class="fl" />
                    </li>
                    <li>
                        <span class="fl">password:</span>
                        <input type="password" id="va4" name="customer_password" class="fl" />
                    </li>
                    <li>
                        <span class="fl">e-email:</span>
                        <input type="text"id="va5" name="customer_email" class="fl" />
                    </li>
                    <li>
                        <span class="fl">tel:</span>
                        <input type="text" id="va6" name="customer_phone" maxlength="11" oninput="value=value.replace(/[^\d]/g,'')" class="fl" />
                    </li>
<%--                    // collect the information //--%>
                    <li>
                        <span class="fl">gender:</span>
                        <select id="va7" class="fl" name="sex">
                            <option value=1>male</option>
                            <option value=0>female</option>
                        </select>
                    </li>
                </ul>
                <div style="margin-bottom: 1px;">
                    <input type="button" onclick="x()" value="sign up" style="width: unset !important;
    color: #fff !important;
    background: #3370ff !important;
    border-color: #3370ff !important;
    height: 32px !important;
    line-height: 22px !important;
    padding: 4px 11px !important;
    font-size: 14px !important;
    border-radius: 6px !important;
    min-width: 80px !important;">
                </div>
            </div>

            </form>
        </div>
    </div>
</body>
<script>

    function x() {
        const a = document.getElementById("va1").value;
        const a1 = document.getElementById("va2").value;
        const a2 = document.getElementById("va3").value;
        const a3 = document.getElementById("va4").value;
        const a4 = document.getElementById("va5").value;
        const a5 = document.getElementById("va6").value;
        const a6 = document.getElementById("va7").value;
        if (a.length == 0 || a1.length == 0 || a3.length == 0 || a4.length == 0 || a6.length == 0 || a2.length == 0) {
            window.alert("your enter value is empty");
        } else {
            if(a2.length == 16) {
                if (a5.length != 10 && a5.length != 11) {
                    window.alert("your phone is not correct");
                } else {
                    document.getElementById("regi").submit();
                }
            }else{
                window.alert("your license is not correct");

            }
        }
    }

</script>

</html>
