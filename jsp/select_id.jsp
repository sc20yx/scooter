<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=GB18030" pageEncoding="GB18030"%>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="../css/jquery.datetimepicker.css"/>
    <title>cep</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        /* head bar */
        .header {
            width: 100%;
            background-color: #1C1C1C;
            display: flex;
            justify-content: space-between;
            align-items: center;
            height: 80px;
            position: fixed;
            top: 0px;
            min-width: 500px;
            z-index: 2;
        }

        /* header left jump */
        .header .left {
            margin-left: 15px;
            width: 400px;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .header a {
            color: white;
            text-align: center;
            text-decoration: none;
        }

        .header a:hover {
            background-color: #ddd;
            color: black;
        }

        /* header login */
        .login {
            margin-right: 15px;
        }

        /* main */
        /* background picture */
        .container {
            background-image: url('../image/banff national park.jpg');
            background-repeat: no-repeat;
            height: 100vh;
            padding: 60px 0;
        }

        /* background size */
        .bgc {
            background-color: #fff;
            width: 80%;
            margin: auto;
            border: 1px solid #eee;
            box-shadow: 0 2px 10px 0 rgb(0 0 0 / 10%);
            min-width: 500px;
        }

        .row {
            position: relative;
            margin: auto;
        }

        .row p {
            font-size: 20px;
        }

        /* Please select start time: */
        .selctime {
            margin-top: 100px;
            width: 100%;
        }

        .selctime input[type="text"] {
            margin-top: 13px;
            width: 150px;
            height: 25px;
            text-indent: 1em;
            line-height: 25px;
        }

        .selctime #open,
        .selctime #close {
            margin-top: 10px;
            width: 50px;
            height: 30px;
            line-height: 25px;
            text-align: center;
            background-color: rgb(233, 147, 147);
            border: none;
            color: #fff;
            border-radius: 3px;
            margin-left: 10px;
        }

        /* Station */
        .Station {
            margin-top: 10px;
        }

        .Station p {
            text-align: left;
        }

        .Station input {
            margin-top: 10px;
            width: 200px;
            height: 25px;
            text-indent: 1em;
            line-height: 25px;
        }

        /* Rental Time: */
        .Rental {
            margin-top: 10px;
        }

        .Rental .Rental {
            margin-top: 10px;
            width: 200px;
            height: 25px;
            text-indent: 1em;
            line-height: 25px;
        }

        .Rental input[type="submit"] {
            margin-top: 10px;
            width: 70px;
            height: 30px;
            line-height: 25px;
            text-align: center;
            background-color: rgb(233, 147, 147);
            border: none;
            color: #fff;
            border-radius: 3px;
            margin-left: 10px;
        }

        /* Choose a scooter: */
        .choose {
            margin: 30px 0;
        }

        .choose select {
            width: 200px;
            height: 30px;
            line-height: 30px;
        }

        /* submit button */
        .button {
            width: 70px;
            height: 30px;
            line-height: 25px;
            background-color: rgb(0, 0, 0);
            border: none;
            color: #fff;
            border-radius: 3px;
            position: absolute;
            bottom: -50px;
            right: 0px;
        }

        /* footer */
        .footer {
            width: 100%;
            height: 60px;
            line-height: 60px;
            font-size: 12px;
            text-align: center;
            color: white;
            background-color: #222325;
            border-bottom: 1px solid #f1f1f1;
            min-width: 500px;
            position: fixed;
            bottom: 0;
        }
    </style>
</head>

<body>
<%@ include file="template/top.jsp"%>
<%

    String loc = (String)request.getParameter("location");
    String time = (String)request.getParameter("Time");
    if(time != null && loc != null){
        session.setAttribute("selected_location",loc);
        session.setAttribute("selected_time",time);
    }else{
        loc = (String)session.getAttribute("selected_location");
        time = (String)session.getAttribute("selected_time");

        session.removeAttribute("selected_location");
        session.removeAttribute("selected_time");
    }

    Object[] user_information = (Object[]) session.getAttribute("user_information");
    if(user_information == null){
        user_information = new Object[]{0,0,0,0,0,0,0,0,0};
    }
    int discount = (int)user_information[6] /100;

    //price according to the discount
    String price = "";
    if(time != null) {
        if(discount == 1) {
            if (time.equals("1")) {
                price = "2.99";
            }
            if (time.equals("4")) {
                price = "6.99";
            }
            if (time.equals("24")) {
                price = "9.99";
            }
            if (time.equals("168")) {
                price = "25.99";
            }
        }else{
            if (time.equals("1")) {
                price = "2.691";
            }
            if (time.equals("4")) {
                price = "6.291";
            }
            if (time.equals("24")) {
                price = "8.991";
            }
            if (time.equals("168")) {
                price = "23.391";
            }
        }
    }
%>

    <!-- main -->
    <div class="container">
        <div class="bgc">
            <div class="row" style="margin: 80px 150px 150px 150px;">
                <form id="small" action="/java_2913_web/search_scooter_Servlet" method="post" >
                    <div class="Station">
                        <p>Station:</p>
                        <input class="station" type="text" readonly="readonly" name="location" value=<%=loc%> style="height:30px;width:200px;">
                        <%
                            if (time == null){
                        %>
                        <input type="button" value="search scooter" style="margin-left:10px;margin-top:10px;position:absolute;width:120px;">
                        <%
                        }else{
                        %>
                        <input type="button" value="search scooter" onclick="smallclick()" style="margin-left:10px;margin-top:10px;position:absolute;width:120px;">
                        <%
                            }
                        %>
                    </div>
                </form>
                <br>
                <form action="/java_2913_web/user_book_Servlet" method="post" id="large">
                    <div class="selctime">
                        <p>Please select start time:</p>
                        <input type="text" name="ordered_time" value="2022/05/04 15:20" id="datetimepicker4" />
                        <input id="open" type="button" value="open" />
                        <input id="close" type="button" value="close" />
                    </div>
                    <br>

                    <!-- Rental -->
                    <div class="Rental">
                        <p>Rental Time:</p>
                        <input class="Rental Time" type="text" name="ordered_duration" value="<%=time%>">
                    </div>
                    <!-- Dropdown box -->
                    <div class="choose">
                        <p>Choose a scooter:</p>
                        <select name="scooter_id" id="scooter_check" class=" ID select form-control" form="large">
                            <%
                                List<Object[]> scoot = (List<Object[]>)session.getAttribute("user_reserve_scooter");

                                if(scoot != null){
                                    for(int i = 0;i < scoot.size(); i++){

                            %>
                            <option value="<%=scoot.get(i)[0]%>">  <%=scoot.get(i)[0]%> </option>
                            <%
                                    }
                                }else{

                            %>
                            <option value="null">none scooter</option>
                            <%
                            }
                                session.removeAttribute("user_reserve_scooter");
                            %>
                        </select>
                    </div>
                    <!-- Price -->
                    <div class="Price">
                        <p>Price:</p>
                        <input class="Price" type="text" value="<%=price%>" name="ordered_price" style="height:30px;width:200px;">
                    </div>
                    <%
                        if (time == null){
                    %>
                    <input class="button" type="submit" value="Confirm">
                    <%
                    }else{
                    %>
                    <input class="button" onclick="largeo()" type="button" value="Confirm">
                    <%
                    }
                    %>
                </form>
            </div>
        </div>

    </div>
<%@ include file="template/footer.jsp"%>
    <!-- script -->
    <script src="../js/datetimepicker.js"></script>
    <script src="../js/jquery.datetimepicker.full.js"></script>
    <script>
       $('#datetimepicker4').datetimepicker();
	$('#open').click(function(){
		$('#datetimepicker4').datetimepicker('show');
	});
	$('#close').click(function(){
		$('#datetimepicker4').datetimepicker('hide');
	});
	$('#reset').click(function(){
		$('#datetimepicker4').datetimepicker('reset');
	});
    function smallclick(){
        document.getElementById("small").submit();
    }
    function largeo(){
        var vala = document.getElementById("scooter_check").value;
        if(vala == null || vala == "null"){
            window.alert("please select avalible scooter");
        }else {
            document.getElementById("large").submit();
        }
    }
    </script>

</body>

</html>