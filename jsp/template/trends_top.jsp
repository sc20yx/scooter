<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta name="viewport" content="width=device-width, initial-scale=1">
<%--Dynamic menu assignment according to role--%>
<style>
    body{
        margin:unset !important;
        padding:60px 0 60px 0px;
    }
    .top{
        position: fixed;
        top: 0;
        left: 0;
        height:60px;
        width: 100vw;
        background-color: #1C1C1C;
        display: flex;
        justify-content: space-between;
        align-items: center;
        font-size: 1.5vw;
        padding-right: 1vw;
        z-index: 99999;
    }
    .top ul.downMenu {
        display: flex;
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
    }
    .top .loginName{
        margin-right: 25px;
    }
    .top a{
        margin-left: 2vw;
        display: block;
        line-height: 60px;
        color:#ffffff;
        text-decoration: none;
        text-decoration-line: none;
        text-decoration-style: initial;
        text-decoration-color: initial;
    }

    input[type=submit]{
        width: unset !important;
        color: #fff !important;
        background: #3370ff !important;
        border-color: #3370ff !important;
        height: 32px !important;
        line-height: 22px !important;
        padding: 4px 11px !important;
        font-size: 14px !important;
        border-radius: 6px !important;
        min-width: 80px !important;
    }
    .func_btn{
        display: none !important;
    }
    .top .isShow{
        display: block;
    }
    .top .isHide{
        display: none;
    }
    .top li{
        height:60px;
    }
    @media screen and (max-width: 700px){
        .top ul.downMenu li:not(:first-child) {
            display: none;
        }
        .func_btn{
            display: block !important;
        }
        ul.downMenu.responsive {
            padding-left: 50px;
            position: fixed;
            background-color: #1C1C1C;
            width:100%;
            top:0;
            left:0;
        }
        ul.downMenu.responsive li {
            float: none;
            display: inline !important;
        }
        ul.downMenu.responsive li a:not(.isHide) {
            font-weight: bold;
            display: block !important;
            text-align: left;
        }
        .top .responsive{
            display: unset !important;
        }
        .loginName{
            position: fixed;
            right: 10px;
        }

    }

</style>

<%
    String user_name = (String) session.getAttribute("user_name");
    Character auth = (Character) session.getAttribute("account_type");
    String path = request.getContextPath();
    String Home = path+"/index.jsp";

String userHireScooters = path+"/webpage/rent.jsp";
String userOrders = path+"/webpage/user/order_not_start.jsp";
String Finance = path+"/webpage/admin/order.jsp";
String State = path+"/webpage/select_car.jsp";
String userFeedback = path+"/webpage/user/user.feedback.jsp";
String adminFeedback = path+"/webpage/admin/admin.feedback.jsp";
String stuffHireScooters = path+"/webpage/serverDate.jsp";
String stuffOrders = path+"/webpage/stuff/order_cancel.jsp";
String Email = path+"/webpage/stuff/E-mail.jsp";
String StuffFeedback = path+"/webpage/stuff/stuff_feedback.jsp";

String userHireScootersClass = "isHide";
String userOrdersClass = "isHide";
String FinanceClass = "isHide";
String StateClass = "isHide";
String userFeedbackClass = "isHide";
String stuffHireScootersClass = "isHide";
String stuffOrdersClass = "isHide";
String EmailClass = "isHide";
String adminFeedbackClass = "isHide";
String StuffFeedbackClass = "isHide";

    String User = path+"/webpage/basic_information.jsp";

    if(user_name == null) {/*Not currently logged in Show menu*/
        userHireScooters = path+"/webpage/login.jsp";
        userOrders = path+"/webpage/login.jsp";
        userFeedback = path+"/webpage/login.jsp";
        userHireScootersClass = "isShow";
        userOrdersClass = "isShow";
        userFeedbackClass = "isShow";
        User = path+"/webpage/login.jsp";
        user_name = "login";
    }
    if(auth!= null && auth.equals('0')){//General users
        userHireScootersClass = "isShow";
        userOrdersClass = "isShow";
        userFeedbackClass = "isShow";
    }
    if(auth!= null && auth.equals('1')) {//admin login
        FinanceClass = "isShow";
        StateClass = "isShow";
        adminFeedbackClass = "isShow";
    }
    if(auth!= null && auth.equals('2')) {//stuff login
        stuffHireScootersClass = "isShow";
        stuffOrdersClass = "isShow";
        EmailClass = "isShow";
        StateClass = "isShow";
        StuffFeedbackClass = "isShow";
    }
%>
<div class="top">
  <%-- header--%>
    <ul class="downMenu">
        <li><img src="<%=path%>/image/logo.png" style=" margin-left: 2vw;width:60px;height:60px;" class="active"></li>
        <li><a href="<%=Home%>">Home</a></li>
        <li><a href="<%=userHireScooters%>" class="<%=userHireScootersClass%>">Hire scooters</a></li>
        <li><a href="<%=Finance%>" class="<%=FinanceClass%>">Finance</a></li>
        <li><a href="<%=stuffHireScooters%>" class="<%=stuffHireScootersClass%>">Hire scooters</a></li>
        <li><a href="<%=userOrders%>" class="<%=userOrdersClass%>">Orders</a></li>
        <li><a href="<%=stuffOrders%>" class="<%=stuffOrdersClass%>">Orders</a></li>
        <li><a href="<%=userFeedback%>" class="<%=userFeedbackClass%>">Feedback</a></li>
        <li><a href="<%=Email%>" class="<%=EmailClass%>">E-mail</a></li>
        <li><a href="<%=State%>" class="<%=StateClass%>">State</a></li>
        <li><a href="<%=adminFeedback%>" class="<%=adminFeedbackClass%>">Feedback</a></li>
        <li><a href="<%=StuffFeedback%>" class="<%=StuffFeedbackClass%>">Feedback</a></li>
    </ul>
    <div class="loginName" style="display: flex">
      <%-- function --%>
        <a href="javascript:void(0);" class="func_btn" style="font-size:15px;" onclick="myFunction()">☰</a>
        <a href="<%=User%>"><%=user_name%></a>
    </div>
</div>

<div style="position: fixed;z-index: -1; background-image:url('<%=path%>/image/banff national park.jpg');background-repeat: no-repeat;height: 100vh; width: 100vw;">
</div>
<%@ include file="../template/footer.jsp"%>
<script>
// functionn for downmenu
    function myFunction() {
        document.getElementsByClassName("downMenu")[0].classList.toggle("responsive");
    }
</script>
