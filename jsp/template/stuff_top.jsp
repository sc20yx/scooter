<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    body{
        margin:unset !important;
        padding:60px 0 60px 0px;
    }
    .top{
        position: fixed;
        top: 0;
        left: 0;
        height:60px;
        width: 100vw;
        background-color: #1C1C1C;
        display: flex;
        justify-content: space-between;
        align-items: center;
        font-size: 1.5vw;
        padding-right: 1vw;
        z-index: 99999;
    }
    .top ul.downMenu {
        display: flex;
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
    }
    .top .loginName{
        margin-right: 25px;
    }
    .top a{
        margin-left: 2vw;
        display: block;
        line-height: 60px;
        color:#ffffff;
        text-decoration: none;
        text-decoration-line: none;
        text-decoration-style: initial;
        text-decoration-color: initial;
    }
    .func_btn{
        display: none !important;
    }
    @media screen and (max-width: 700px){
        .top ul.downMenu li:not(:first-child) {
            display: none;
        }
        .func_btn{
            display: block !important;
        }
        ul.downMenu.responsive {
            padding-left: 50px;
            position: fixed;
            background-color: #1C1C1C;
            width:100%;
            top:0;
            left:0;
        }
        ul.downMenu.responsive li {
            float: none;
            display: inline !important;
        }
        ul.downMenu.responsive li a {
            font-weight: bold;
            display: block !important;
            text-align: left;
        }
        .top .responsive{
            display: unset !important;
        }
        .loginName{
            position: fixed;
            right: 10px;
        }

    }

</style>

<%
    String user_name = (String) session.getAttribute("user_name");
    String path = request.getContextPath();
    String Home = path+"/index.jsp";
    String Hirescooters = path+"/webpage/serverDate.jsp";
    String Orders = path+"/webpage/stuff/order_cancel.jsp";
    String Email =path+"/webpage/stuff/E-mail.jsp";
    String State = path+"/webpage/select_car.jsp";
    String Feedback = path+"/webpage/stuff/stuff_feedback.jsp";
    String User = path+"/webpage/basic_information.jsp";

    if(user_name == null) {
        Hirescooters = path+"/webpage/login.jsp";
        Orders = path+"/webpage/login.jsp";
        Email = path+"/webpage/login.jsp";
        State = path+"/webpage/login.jsp";
        Feedback = path+"/webpage/login.jsp";
        User = path+"/webpage/login.jsp";
        user_name = "login";
    }
%>
<div class="top">
  <%-- header for staff --%>
    <ul class="downMenu">
        <li><img src="<%=path%>/image/logo.png" style="margin-left: 2vw;width:60px;height:60px;" class="active"></li>
        <li><a href="<%=Home%>">Home</a></li>
        <li><a href="<%=Hirescooters%>">Hire scooters</a></li>
        <li><a href="<%=Orders%>">Orders</a></li>
        <li><a href="<%=Email%>">E-mail</a></li>
        <li><a href="<%=State%>">State</a></li>
        <li><a href="<%=Feedback%>">Feedback</a></li>
    </ul>
    <div class="loginName" style="display: flex">
      <%-- make a function --%>
        <a href="javascript:void(0);" class="func_btn" style="font-size:15px;" onclick="myFunction()">☰</a>
        <a href="<%=User%>"><%=user_name%></a>
    </div>
</div>



<div style="position: fixed;z-index: -1; background-image:url('<%=path%>/image/banff national park.jpg');background-repeat: no-repeat;height: 100vh; width: 100vw;">
</div>

<%@ include file="../template/footer.jsp"%>
<script>
// for function 
    function myFunction() {
        document.getElementsByClassName("downMenu")[0].classList.toggle("responsive");
    }
</script>
