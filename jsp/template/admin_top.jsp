<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    body{
        margin:unset !important;
        padding:60px 0 60px 0px;
    }
    .top{
        position: fixed;
        top: 0;
        left: 0;
        height:60px;
        width: 100vw;
        background-color: #1C1C1C;
        display: flex;
        justify-content: space-between;
        align-items: center;
        font-size: 1.5vw;
        padding-right: 1vw;
        z-index: 99999;
    }
    .top .left{
        display: flex;
    }
    .top a{
        margin-left: 2vw;
        display: block;
        line-height: 60px;
        color:#ffffff;
        text-decoration: none;
        text-decoration-line: none;
        text-decoration-style: initial;
        text-decoration-color: initial;
    }
    .top .loginName{
        margin-right: 25px;
    }
</style>

<%
    String user_name = (String) session.getAttribute("user_name");
    String path = request.getContextPath();
    String Home = path+"/index.jsp";
    String Finance = path+"/webpage/admin/order.jsp";
    String State = path+"/webpage/select_car.jsp";
    String Feedback =path+"/webpage/admin/admin.feedback.jsp";
    String user = path+"/webpage/basic_information.jsp";
    if(user_name == null) {
        Finance = path+"/webpage/login.jsp";
        State = path+"/webpage/login.jsp";
        Feedback = path+"/webpage/login.jsp";
        user = path+"/webpage/login.jsp";
        user_name = "login";
    }
%>
<div class="top">
  <%-- header for admin --%>
    <div class="left">
        <img src="<%=path%>/image/logo.png" style="margin-left:2vw;width:60px;height:60px;" />
        <a href="<%=Home%>">Home</a>
        <a href="<%=Finance%>">Finance</a>
        <a href="<%=State%>">State</a>
        <a href="<%=Feedback%>">Feedback</a>
    </div>
    <div class="loginName">
        <a href="<%=user%>"><%=user_name%></a>
    </div>
</div>
<div style="position: fixed;z-index: -1; background-image:url('<%=path%>/image/banff national park.jpg');background-repeat: no-repeat;height: 100vh; width: 100vw;">
</div>
<%@ include file="../template/footer.jsp"%>
