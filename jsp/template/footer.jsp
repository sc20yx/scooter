<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    .content ,.container,.content2{
        margin-top: 20px;
    }

    .custom-btn{
        width: unset !important;
        color: #fff !important;
        background: #3370ff !important;
        border-color: #3370ff !important;
        height: 32px !important;
        line-height: 22px !important;
        padding: 4px 11px !important;
        font-size: 14px !important;
        border-radius: 6px !important;
        min-width: 70px !important;
    }
    .default-custom-btn{
        width: unset !important;
        color: #1f2329 !important;
        background: #fff !important;
        border-color: #d0d3d6 !important;
        height: 32px !important;
        line-height: 22px !important;
        padding: 4px 11px !important;
        font-size: 14px !important;
        border-radius: 6px !important;
        min-width: 70px !important;
    }
    input[type=submit] ,input[type=button],button:not(.xdsoft_prev):not(.xdsoft_today_button):not(.xdsoft_next),a.button{
        width: unset !important;
        color: #fff !important;
        background: #000000 !important;
        border-color: #000000 !important;
        height: 32px !important;
        line-height: 22px !important;
        padding: 4px 11px !important;
        font-size: 14px !important;
        border-radius: 6px !important;
        min-width: 80px !important;
    }
    button > a,a[type=button]{
        color:#fff !important;
    }

</style>
<%-- footer style --%>
<div style="position: fixed;bottom: 0;width: 100%; height: 60px;z-index: 999999;
line-height: 60px;font-size:1vw;text-align: center;color: white; background-color: #222325;border-bottom: 1px solid #f1f1f1;">
    2022 cep.com
</div>
