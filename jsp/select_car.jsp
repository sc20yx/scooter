<%@ page import="java.util.List" %>
<%@ page import="comp2913.web.Dao_for_mysql" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Connection" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html lang="en">
<meta http-equiv="Content-Type" Content="text/html;charset=utf8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/select_car.min.css" />
<link rel="stylesheet" href="../css/select_car.css">

<head>
    <title>cep</title>
    <style>
        .pay-btn {
            display: inline-block;
            height: 48px;
            width: 80%;
            line-height: 48px;
            color: white;
            background-color: #eb5b00;
            text-decoration: none;
            text-align: center;
            border-radius: 12px;
        }

        .pay-btn:hover {
            text-decoration: none
        }

        .date-widget{
            width: 80%;
            height: 48px;
            line-height: 48px;
            font-size: 18px;
            margin-bottom: 12px;
            font-weight: bold; cursor: pointer;padding: 0 20px;border-radius: 12px;
        }
        .buy-btn{
            cursor: pointer;
            position: absolute;
            height: 48px;
            width: 48px;
            border-radius: 50%;
            background-color: coral;
            text-align: center;
            line-height: 48px;
            font-size: 24px;
            color: white;
            left: 15px;
            top: 15px;
            z-index: 999;
            border: 1px solid #eee;
            box-shadow: 0 2px 10px 0 rgb(0 0 0 / 10%);
        }
        .buycar-panel{
            background-color: #fff;
            border: 1px solid #eee;
            position: relative;
            box-shadow: 0 2px 10px 0 rgb(0 0 0 / 10%);
            margin-bottom: 15px;
            padding: 15px;
        }
        .text{
            width:100px;
            border:0px;
		    outline:none;
        }
    </style>
</head>


<body>
<%@ include file="template/trends_top.jsp"%>
    <%
        Connection conn = Dao_for_mysql.getConn();
        //admin search all scooter
        ResultSet resui = Dao_for_mysql.getAll(conn,"select * from scooter;");
        List<Object[]> scooter_li = new ArrayList<Object[]>();
        try {
            if(resui != null) {
                while (resui.next()) {
                    int id = resui.getInt("scooter_id");
                    String scooter_rantable = resui.getString("scooter_reser_status");
                    String scooter_addre = resui.getString("adress_name");
                    String scooter_status = resui.getString("scooter_status");
                    int price_scooter = resui.getInt("scooter_price");


                    scooter_li.add(new Object[]{id, scooter_rantable, scooter_status,scooter_addre,price_scooter});
                }
            }
            resui.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    %>

    <div class="container">
        <div style="background-color: #fff;
        border: 1px solid #eee;
        margin: 15px 0px;
        position: relative;
        box-shadow: 0 2px 10px 0 rgb(0 0 0 / 10%);">
            <form id="add_scooter" action="/java_2913_web/add_scooter_Servlet" method="post"></form>
            <input class="buy-btn" type="" onclick="x()" value="+">

            <div class="row" style="margin: 80px 15px 15px 15px;">
                <%
                    if(scooter_li == null){

                    }else{
                        String rantal = "";
                        String car_ava = "";
                        for(int i = 0;i < scooter_li.size();i++){
                            if(((String)scooter_li.get(i)[1]).equals("0")){
                                rantal = "available";
                            }
                            if(((String)scooter_li.get(i)[1]).equals("1")){
                                rantal = "unavailable";
                            }
                            if(((String)scooter_li.get(i)[2]).equals("0")){
                                car_ava = "available";
                            }

                            if(((String)scooter_li.get(i)[2]).equals("1")){
                                car_ava = "unavailable";
                            }
                            if(((String)scooter_li.get(i)[2]).equals("2")){
                                car_ava = "broken";
                            }
                %>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="buycar-panel">
                        <form action="./information_car.jsp" method="post">
                            <input type="hidden" value="<%=scooter_li.get(i)[0]%>" name="id">
                            <input type="hidden" value="<%=scooter_li.get(i)[1]%>" name="reservable">
                            <input type="hidden" value="<%=scooter_li.get(i)[2]%>" name="car_avalib">
                            <input type="hidden" value="<%=scooter_li.get(i)[3]%>" name="location_scooter">
                            <input type="hidden" value="<%=scooter_li.get(i)[4]%>" name="scoo_price">
                            <p>Scooter ID：<%=scooter_li.get(i)[0]%></p>
                            <p>Rentable: <input type="text" value="<%=rantal%>" class="text" readonly="readonly"></p>
                            <p>Status：<input type="text" value="<%=car_ava%>" class="text"readonly="readonly"></p>
                            <p>Location：<input type="text" readonly="readonly" value="<%=scooter_li.get(i)[3]%>" class="text"></p>
                            <br><br>
                            <div style="text-align: right;">
                                <input type="submit" class="btn btn-sm btn-warning" value="edit">
                            </div>
                        </form>
                    </div>
                </div>
                <%
                        }
                    }
                %>
            </div>
        </div>
    </div>
<%@ include file="template/footer.jsp"%>
</body>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script>
    function x() {
        document.getElementById("add_scooter").submit();

    }
     $(function(){
        $("#getCar").on("click",function(){
            window.location.href="index.html";
        });
    });
</script>
</html>