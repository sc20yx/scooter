<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="always" name="referrer">
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>serverDate</title>
    <link rel="stylesheet" href="../css/reset.css" type="text/css" />
    <link rel="stylesheet" href="../css/style1.css" type="text/css" />
    <link rel="stylesheet" href="../css/response.css" type="text/css" />
    <style>
        .con_list ul li{
            display: block;
            justify-content: center;
        }
    </style>
</head>

<body >

<%@ include file="./template/stuff_top.jsp"%>
    <!--wrapper -->
    <div id="wrapper">
        <form id="small" action="/java_2913_web/search_scoo_staf_Servlet" method="post">
            <input id="useful" type="hidden" name="location">
        </form>

        <div class="content mar" style="background-color: #ffffff">
            <form id="staff" action="/java_2913_web/staff_book_Servlet" method="post">
                <div class="con_list serverBox ">
                    <h2 class="mar">scooter booking</h2>
                    <h2 class="mar">Data collect</h2>
                    <ul class="mar" style="border-bottom: 1px solid #000;">
                        <li class="noBorder">
                            <span class="fl">username:</span>
                            <input type="text" id="va1" name="name_user" class="fl" />
                        </li>
                        <li class="noBorder">
                            <span class="fl">license:</span>
                            <input type="text" id="va2" name="user_lisence" maxlength="16" class="fl" />
                            </li>
                        <li class="noBorder">
                            <span class="fl">phone:</span>
                            <input type="text" id="va3" name="staf_phone" maxlength="11" oninput="value=value.replace(/[^\d]/g,'')" class="fl" />
                        </li>
                        <li class="noBorder re">
                          <span class="fl">date:</span>
                            <input id="ordered" name="ordered_time" type="date">
                        </li>
                        <li class="noBorder">
                            <span class="fl">time:</span>
                            <select class="fl"name="ordered_duration">
                                <option value="1" selected>1 hour</option>
                                <option value="4">4 hour</option>
                                <option value="24">1 day</option>
                                <option value="168">1 week</option>
                            </select>
                        </li>
<%--                        // make the text to collect information //--%>
                        <li>
                            <span class="fl">location:</span>
                            <select id="not_use" class="fl" name="location">
                                <option value="Trinity_centre" selected>Trinity centre</option>
                                <option value="Train_station">Train station</option>
                                <option value="Merrion_centre">Merrion centre</option>
                                <option value="LRI_hospital">LRI hospital</option>
                                <option value="UoL_Edge_sports_centre">UoL Edge sports centre</option>
                            </select>
                            <input type="button" value="search" onclick="smallclick()" style="margin-left:10px;margin-top:10px;position:absolute;">
                        </li>
                        <li class="noBorder">
                            <span class="fl" >scooter id:</span>
                            <select id="va4" class="fl" name="scooter_id">
                                <%
                                    List<Object[]> scoot = (List<Object[]>)session.getAttribute("user_reserve_scooter");

                                    if(scoot != null){
                                        for(int i = 0;i < scoot.size(); i++){
                                %>
                                <option value="<%=scoot.get(i)[0]%>">  <%=scoot.get(i)[0]%> </option>
                                <%
                                    }
                                }else{

                                %>
                                <option value="none">none scooter</option>
                                <%
                                    }
                                    session.removeAttribute("user_reserve_scooter");
                                %>
                            </select>
                        </li>
                    </ul>
                    <div style="text-align: center;margin-bottom: 10px;">
                        <input type="button" onclick="x()" value="submit">
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
<script>
    function smallclick(){
        var a = document.getElementById("not_use").value;
        document.getElementById("useful").value = a;
        document.getElementById("small").submit();
    }


    function x() {
        var a = document.getElementById("va1").value;
        var a1 = document.getElementById("va2").value;
        var a2 = document.getElementById("va3").value;
        var a3 = document.getElementById("ordered").value;
        var a4 = document.getElementById("va4").value;

        if (a.length == 0 || a1.length == 0 || a3.length == 0 || a4.length == 0 || a == null || a1 == null || a3 == null || a4 == null) {
            window.alert("your enter value is empty");
        } else {
            if(a1.length ==16) {
                if (a2.length != 10 && a2.length != 11) {
                    window.alert("your phone is not correct");
                } else {
                    document.getElementById("staff").submit();
                }
            }else{
                window.alert("your license is not correct");

            }
        }
    }

</script>
</html>
