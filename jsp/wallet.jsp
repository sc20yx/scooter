<%@ page import="comp2913.web.Dao_for_mysql" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.List" %>
<%@ page import="javax.swing.*" %>
<%@ page import="java.sql.Connection" %>
<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>walletedit</title>

    <link rel="stylesheet" href="../css/wallet.css">
</head>



<body>
<%@ include file="template/top.jsp"%>
        <div class="w">
            <h2>wallet</h2>
            <div class="content">
<%--get the innformation from database--%>
                <div class="left_box">
                    <ul>
                        <%
                            Connection conn = Dao_for_mysql.getConn();
                            int id_user = (int)request.getSession().getAttribute("user__id");
                            ResultSet a = Dao_for_mysql.getAll(conn,"select * from card where customer_id = '"+id_user+"';");
                            List<Object[]> cards = new ArrayList<Object[]>();
                            try {
                                if(a != null) {
                                    while (a.next()) {
                                        String card_num = a.getString("card_number");
                                        String card_na = a.getString("card_name");
                                        cards.add(new Object[]{card_na,card_num});
                                    }
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }

                            conn.close();
                            if(a == null){

                            }else{
                                for(int i = 0;i< cards.size();i++){


                        %>


                        <%--first card--%>
                        <li class="first">
                            <form action="/java_2913_web/delete_card_Servlet" method="post">
                                <div class="info">
                                    <input  class="card" style="height:40px;" type="text" name="cards_name" value="<%=cards.get(i)[0]%>" readonly="readonly">
                                    <br>
                                    <br>
                                    <input class="card" style="height:40px;" type="text" name="cards_number" value="<%=cards.get(i)[1]%>" readonly="readonly">
                                    <br>
                                    <input type="submit" class="edit" value="Delete">
                                </div>
                            </form>
                        </li>
                        <%
                                }

                            }
                            a.close();
                        %>

                    </ul>
                </div>
                <form id="addc" action="/java_2913_web/add_card_servlet" method="post" style="width:400px;">
<%--                    add card function--%>
                    <div class="right_box" style="padding:40px 0px;background: #fff;text-align: center;">
                        <h4>Card number</h4>
                        <input type="text" id="va1" style="height:40px;" name="card_number" maxlength="16" oninput="value=value.replace(/[^\d]/g,'')">
                        <h4>Name on Card</h4>
                        <input type="text" id="va2" style="height:40px;" name="card_name">

                        </ul>
                        <ul class="btn">
                            <li style="margin-right: -31px;"><button style=""><a style="color: black;" href="./wallet2.jsp">Cancel</a> </button></li>
                            <input type="button" onclick="x()" name="addcard" value="addcard" style="background-color: rgba(255, 191, 99, 0.69); width: 80px; font-size: 20px">

                        </ul>
                    </div>
                </form>
            </div>
        </div>


</body>
<script>
    function x() {
        var a = document.getElementById("va1").value;
        var a1 = document.getElementById("va2").value;

        if (a1.length == 0) {
            window.alert("your enter value is empty");
        } else {
            if(a.length != 16){
                window.alert("your card number is not correct");
            }else {
                document.getElementById("addc").submit();
            }
        }
    }

</script>
<%--function for Responsive Design --%>
<script>
    function myFunction() {
        document.getElementsByClassName("downMenu")[0].classList.toggle("responsive");
    }
</script>

</html>
