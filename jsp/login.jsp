<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta content="always" name="referrer">
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>login</title>
    <link rel="stylesheet" href="../css/reset.css" type="text/css" />
    <link rel="stylesheet" href="../css/style1.css" type="text/css" />
    <link rel="stylesheet" href="../css/response.css" type="text/css" />

    <style>
        /*iphone style*/
        @media (min-width: 400px) and (max-width:767px) {
            .con_list ul{
                width: 100% !important;
            }
            .con_list{
                padding: unset !important;
            }
            .content{
                width: 100% !important;
            }
            .butBox {
                padding: 0 !important;
                height: 60px !important;
                width: 100% !important;
            }

        }
    </style>
</head>

<body >
<%@ include file="template/top.jsp"%>


    <!--wrapper -->
    <div id="wrapper">
        <!--contentbox  -->



        <div class="content mar">
            <form id="indo" action="/java_2913_web/login_Servlet" method="post">
                <div class="con_list">
                    <ul class="mar" style="margin-top: 160px;">
                        <li>
                            <span class="fl">username:</span>
                            <input id="pass" type="text" name="username" class="fl" />
                        </li>
                        <li>
                            <span class="fl">password:</span>
                            <input id="word" type="password" name="password" class="fl" />
                        </li>
<%--                        gave text bar to log in --%>
                    </ul>
                </div>
                <div class="butBox mar">
                    <a class="button fl normal " href="./signUp.jsp">sign up</a>
                    <a class="button fl normal" href="./findPassword.jsp">forgot ?</a>
                    <a type="button" onclick="x()" class="button fr normal" value="login">login</a>
                </div>
            </form>
        </div>
    </div>
</body>
<script>
    function x() {
        var a = document.getElementById("pass").value;
        var a1 = document.getElementById("word").value;
        if (a.length == 0 || a1.length == 0) {
            window.alert("your enter value is empty");
        } else {
            document.getElementById("indo").submit();
        }
    }
</script>
</html>
