// feedback's js

var problem_level_data_list = {
    // List of problem levels
    'Order': ['Order disappeared', 'Doubts about the order', 'Order status', 'Other'],
    'Payment': ['Time of use issues', 'Unable to pay problem', 'Payment amount issues', 'Other'],
    'Scooter': ['Book a scooter', 'Scooter power', 'Scooter breakdown', 'Other'],
    'Login': ['Change of password', 'Unable to login', 'change of personal information', 'Other'],
    'Service': ['service issues for stuff', 'Scooter experience', 'Experience using the websites', 'Other'],
    'Other': ['Other', ]
}

function gen_select(problem_level_data_list, problem) {
    var options = problem_level_data_list[problem]; // get ProblemDetail
    console.log(options);
    // Generate select
    var html = '<select name="problem_detail" id="problem_type" class="select form-control">';
    for (let index = 0; index < options.length; index++) {
        const element = options[index];
        html += '<option value="' + element + '" >' + element + '</option>';
    }
    html += '</select>'
    return html
}
