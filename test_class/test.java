package comp2913.test;

import comp2913.web.Dao_for_mysql;
import org.junit.Test;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class test {

    @Test
    public void test_for_database() throws Exception {
        //this test is used to ensure that database can work correctly
        Connection as = Dao_for_mysql.getConn();
        int test_count = 0;
        Object[] a = {"aaaa","2020/10/11 18:00","null","5","0","10"};
        Dao_for_mysql.insertOrupdate(as,"insert into scooter(adress_name,scooter_reserve_time,scooter_reserve_list,scooter_price,scooter_status,scooter_reser_status) value(?,?,?,?,?,?)",a);

        ResultSet re = Dao_for_mysql.getAll(as,"select * from scooter where scooter_reser_status = '10';");
        while(re.next()){
            if(re.getString("adress_name").equals("aaaa")){
                test_count++;
            }
        }

        a = new Object[]{"wowowowowo", "aaaa"};
        Dao_for_mysql.insertOrupdate(as,"UPDATE scooter SET adress_name=? WHERE adress_name=?;",a);

        ResultSet re_update = Dao_for_mysql.getAll(as,"select * from scooter where adress_name = 'wowowowowo';");
        while(re_update.next()){
            if(re_update.getString("adress_name").equals("wowowowowo")){
                test_count++;
            }
        }

        int test_dele_count = 0;
        Dao_for_mysql.delete(as,"delete from scooter where adress_name = 'wowowowowo'");

        ResultSet re_dele = Dao_for_mysql.getAll(as,"select * from scooter;");
        while(re_dele.next()){
            if(re_dele.getString("adress_name").equals("wowowowowo")){
                test_dele_count++;
            }
        }

        if(test_dele_count == 0){
            test_count++;
        }

        assert test_count == 3;

    }


    @Test
    public void test_calendar(){
        Calendar c = Calendar.getInstance();
//        SimpleDateFormat f=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        System.out.println(f.format(c.getTime())+" time");
        for(int i = 0;i<12;i++){
            c.add(Calendar.MONTH,1);
//            System.out.println(f.format(c.getTime())+" time");
//            System.out.println(c.get(Calendar.MONTH));
        }

        Calendar scoo_tt = Calendar.getInstance();

        long aTime1 = c.getTimeInMillis();
        long bTime1 = scoo_tt.getTimeInMillis();
        long cTime1 = aTime1 - bTime1;

        long sTime1 = cTime1 / 1000;//second
        long mTime1 = sTime1 / 60;//mini
        long hTime1 = mTime1 / 60;//hour
        long dTime1 = hTime1 / 24;//day


        assert dTime1 <= 366 && dTime1 >= 364;
        System.out.println(dTime1);

    }

    @Test
    public void check_resource_file() throws Exception{
        //this test is used to ensure web part file is in the correct path
        File dir = new File("src\\main\\webapp");
        List<String> fileNames = new ArrayList<>();
        findFileList(dir,fileNames );
        int count = 0;
        for (String value :  fileNames) {
            if(check_resourc_string(value) == true){
                count = count + 1;
            }
        }

        if(count == 125){
            System.out.println("success");
        }else{
            throw new Exception();
        }
    }

    @Test
    public void check_servlet_file() throws Exception{
        //this test is used to ensure servlet part file is in the correct path
        File dir = new File("src\\main\\java");
        List<String> fileNames = new ArrayList<>();
        findFileList(dir,fileNames);
        int count = 0;
        for (String value :  fileNames) {
            if(check_servlet_string(value) == true){
                count = count + 1;
            }
        }

        if(count == 24){
            System.out.println("success");
        }else{
            throw new Exception();
        }
    }
    public boolean check_servlet_string(String value){
        String[] check_list = {"Add_card_Servlet.java","Add_scooter_Servlet.java","Book_confirm_Servlet.java","Cancel_appointment_Servlet.java","Dao_for_mysql.java","Delete_card_Servlet.java","Down_feedback_Servlet.java","Edit_Personal_information_Servlet.java","Login_Servlet.java","Logout_Servlet.java","Longer_appointment_Servlet.java","Pay_Servlet.java","Register_Servlet.java","Scooter_update_Servlet.java","Search_orderByid_Servlet.java","Search_scooter_Servlet.java","Search_scoo_staf_Servlet.java","Send_Feedback_Servlet.java","Send_massage_Servlet.java","Solve_feedback_Servlet.java","Staff_book_Servlet.java","Update_password_Servlet.java","Up_feedback_Servlet.java","User_book_Servlet.java"};
        for(int i = 0; i < check_list.length;i++){
            if(value.indexOf(check_list[i]) != -1){
                return true;
            }
        }
        return false;
    }

    public boolean check_resourc_string(String value){
        String[] check_list = {"base.css","base_feedback.css","base_feedback_common.css","bootstrap-grid.css_feedback.map","bootstrap-grid.min.css_feedback.map","bootstrap-grid.min_feedback.css","bootstrap-grid_feedback.css","bootstrap-reboot.css_feedback.map","bootstrap-reboot.min.css_feedback.map ","bootstrap-reboot.min_feedback.css","bootstrap-reboot_feedback.css","bootstrap.css_feedback.map","bootstrap.min.css_feedback.map","bootstrap.min_feedback.css","bootstrap_feedback.css","feedback.css","form.css","form_feedsback.css","global.css","index.css","jquery.datetimepicker.css","order.css","order1.css","order_base.css","order_cancel.css","payment.css","reset.css","response.css","secussed.css","select_car.css","select_car.min.css","style.css","style1.css","sz_boot.css","wallet.css","1.jpg","2.jpg","askForHelp.png","background.webp","balance.png","banff national park.jpg","bookScooter.png","box1.png","box2.png","box3.png","check.png","edge.png","failed.png","hospital.png","list.svg","logo.jpg","logo.png","logout.png","menu.svg","morrison.png","ok.jpg","order.png","train.png","trinity.png","viewYourOrder.png","wallet.png","index.jsp","bootstrap.bundle.js_feedback.map","bootstrap.bundle.min.js_feedback.map","bootstrap.bundle.min_feedback.js","bootstrap.bundle_feedback.js","bootstrap.js_feedback.map","bootstrap.min.js_feedback.map","bootstrap.min_feedback.js","bootstrap_feedback.js","chart_jqeury.js","datetimepicker.js","echarts.min.js","feedback.js","information.js","jquery.datetimepicker.full.js","jquery.min.js","rent.min.js","sz_boot.js","activation.jar","commons-fileupload-1.2.1.jar","commons-io-1.3.2.jar","json_simple-1.1.jar","jspsmartupload.jar","jtds-1.2.2 .jar","jxl.jar","mail.jar","msbase.jar","mssqlserver.jar","msutil.jar","mysql-connector-java-8.0.20.jar","sqljdbc.jar","sqljdbc4.jar","zxing-1.6-core.jar","web.xml","admin.feedback.jsp","chart.jsp","order.jsp","basic_information.jsp","failure.jsp","findPassword.jsp","informationedit.jsp","information_car.jsp","login.jsp","payment.jsp","rent.jsp","secussed.jsp","select_car.jsp","select_id.jsp","serverDate.jsp","signUp.jsp","E-mail.jsp","order_cancel.jsp","stuff_feedback.jsp","stuff_order_pay.jsp","admin_top.jsp","background.jsp","footer.jsp","stuff_top.jsp","top.jsp","trends_top.jsp","order_completed.jsp","order_not_start.jsp","order_underway.jsp","user.feedback.jsp","wallet.jsp","wallet2.jsp"};
        for(int i = 0; i < check_list.length;i++){
            if(value.indexOf(check_list[i]) != -1){
                return true;
            }
        }
        return false;
    }

    public static void findFileList(File dir, List<String> fileNames) {
        if (!dir.exists() || !dir.isDirectory()) {
            return;
        }
        String[] files = dir.list();
        for (int i = 0; i < files.length; i++) {
            File file = new File(dir, files[i]);
            if (file.isFile()) {
                fileNames.add(dir + "\\" + file.getName());
            } else {
                findFileList(file, fileNames);
            }
        }
    }


}

